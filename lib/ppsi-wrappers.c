#include <stdint.h>
#include <hal_exports.h>
#include <wrc_ptp.h>
#include <syscon.h>
#include <endpoint.h>
#include <softpll_ng.h>
#include <ptpd_netif.h>

/* Following code from ptp-noposix/libposix/freestanding-wrapper.c */

uint64_t ptpd_netif_get_msec_tics(void)
{
  return timer_get_tics();
}

static int read_phase_val(hexp_port_state_t *state, uint8_t port)
{
  int32_t dmtd_phase;

  if(spll_read_ptracker(port, &dmtd_phase, NULL))
  {
    state->phase_val = dmtd_phase;
    state->phase_val_valid = 1;
  }
  else
  {
    state->phase_val = 0;
    state->phase_val_valid = 0;
  }

  return 0;
}

extern uint32_t cal_phase_transition[2];
extern int32_t sfp_alpha[2];

int halexp_get_port_state(hexp_port_state_t *state, const char *port_name)
{
  state->valid         = 1;

  //WR-LEN
  int port = atoi(&port_name[2]);

  int wrc_mode = wrc_ptp_get_mode();

  if(port == 0)
	  if(wrc_mode == WRC_SLAVE_WR0)
		  state->mode = HEXP_PORT_MODE_WR_SLAVE;
	  else
		  state->mode = HEXP_PORT_MODE_WR_MASTER;
  else
	  if(wrc_mode == WRC_SLAVE_WR1)
		  state->mode = HEXP_PORT_MODE_WR_SLAVE;
	  else
		  state->mode = HEXP_PORT_MODE_WR_MASTER;

  ep_get_deltas( &state->delta_tx, &state->delta_rx, port);
  read_phase_val(state, port);
  state->up            = ep_link_up(NULL, port);
  state->tx_calibrated = 1;
  state->rx_calibrated = 1;
  state->is_locked     = spll_check_lock(0);
  state->lock_priority = 0;
  spll_get_phase_shift(port, NULL, (int32_t *)&state->phase_setpoint); //WR-LEN. Always pointing to to the 0 channel. I think.
  state->clock_period  = REF_CLOCK_PERIOD_PS;
  state->t2_phase_transition = cal_phase_transition[port];
  state->t4_phase_transition = cal_phase_transition[port];
  get_mac_addr(state->hw_addr, port);
  state->hw_index      = 0;
  state->fiber_fix_alpha = sfp_alpha[port];

  return 0;
}

int ptpd_netif_get_dmtd_phase(wr_socket_t *sock, int32_t *phase)
{
        if(phase)
                return spll_read_ptracker(0, phase, NULL);
        return 0;
}
