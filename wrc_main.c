/*
 * This work is part of the White Rabbit project
 *
 * Copyright (C) 2011,2012 CERN (www.cern.ch)
 * Author: Tomasz Wlostowski <tomasz.wlostowski@cern.ch>
 * Author: Grzegorz Daniluk <grzegorz.daniluk@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 */
#include <stdio.h>
#include <inttypes.h>

#include <stdarg.h>

#include <wrc.h>
#include <w1.h>
#include "syscon.h"
#include "uart.h"
#include "endpoint.h"
#include "minic.h"
#include "pps_gen.h"
#include "ptpd_netif.h"
#include "i2c.h"
#include "eeprom.h"
#include "softpll_ng.h"
#include "onewire.h"
#include "pps_gen.h"
#include "shell.h"
#include "lib/ipv4.h"
#include "rxts_calibrator.h"
#include "sfp.h"

#include "wrc_ptp.h"
#include "eth_subsystem.h"

#include "wrc.h"


#ifdef CONFIG_ZEN_BOARD
	#include "wr_zen_i2c.h"
#endif

#ifdef CONFIG_ZEN_CTA_BOARD
	#include "wr_zen_cta_gpio.h"
#endif

#ifdef CONFIG_EXPANSION_SPI
	#include "exp_spi.h"
#endif

#ifdef CONFIG_LENTRIG
	#include "len_fine_tune.h"
#endif

int wrc_ui_mode = UI_SHELL_MODE;
int wrc_phase_tracking = 1;
int ext_refresh_time = TICS_PER_SECOND; //One second by default

char board_name_ext[13];

struct wrc_stats *wrc_stats_head=0x1F700; //See arch/lm32/ram.ld

#ifdef CONFIG_ZEN_BOARD
	static int check_and_set_led=0;
#endif


static void wr_board_name_initialize(){
	#ifdef CONFIG_LEN_BOARD
		strcpy(board_name_ext, "LEN board    ");
	#elif CONFIG_ZEN_BOARD
		strcpy(board_name_ext, "ZEN board    ");
	#elif CONFIG_ZEN_CTA_BOARD
		strcpy(board_name_ext,"ZEN-CTA board");
	#else
		strcpy(board_name_ext, "Bare Software");
#endif
}
///////////////////////////////////
//Calibration data (from EEPROM if available)
//default values for both ports if could not read EEPROM

int32_t sfp_alpha[2] = {SFP_ALPHA_DEFAULT, SFP_ALPHA_DEFAULT};
int32_t sfp_deltaTx[2] = {SFP_DELTATX_DEFAULT, SFP_DELTATX_DEFAULT};
int32_t sfp_deltaRx[2] = {SFP_DELTARX_DEFAULT, SFP_DELTARX_DEFAULT};
uint32_t cal_phase_transition[2] = {2389, 2389};

static void wrc_initialize()
{
	int i;
	uint8_t mac_addr[wrlen_num_ports][6];

	sdb_find_devices();
	uart_init_sw();
	uart_init_hw();

	mprintf("WRC-DUAL-PORT: %s\n", board_name_ext);
	mprintf("WR Core: starting up...\n");


	pp_printf("-----------------------\n");

	timer_init(1);
	wrpc_w1_init();
	wrpc_w1_bus.detail = ONEWIRE_PORT;
	w1_scan_bus(&wrpc_w1_bus);

#if defined(CONFIG_ZEN_BOARD) || defined(CONFIG_ZEN_CTA_BOARD)
	/* GPIO initialization */

	wr_zen_gpio_init();
	pp_printf("------------------------\n");

#endif

#ifdef CONFIG_EXPANSION_SPI
	exp_spi_init();
	pp_printf("------------------------\n");
#endif

#ifdef CONFIG_XADC_TEMP
	xadc_temp_init();
#endif


#ifdef CONFIG_ZEN_BOARD
	unsigned char d;

	/*initialize I2C bus*/
	mi2c_init(WRPC_FMC_I2C);

	/* INIT PCA9548A  */
	d = read_i2c_switch(WRPC_FMC_I2C,I2C_SWITCH_ADDR);

	if (d >= 0)
		mprintf("I2C Switch initial configuration: 0x%x!!\n",(unsigned char) d);

	if (configure_i2c_switch(WRPC_FMC_I2C,I2C_SWITCH_ADDR,I2C_SWITCH_DFL_CFG) != 0)
		mprintf("Error: I2C Switch could not be configured properly \n");
	else {
		d = read_i2c_switch(WRPC_FMC_I2C,I2C_SWITCH_ADDR);

		if (d >= 0)
			mprintf("I2C Switch configuration updated: 0x%x!!\n",(unsigned char) d);

		pp_printf("------------------------\n");
	}
	
	/* Init the I2C SFP GPIOs */
	wr_zen_i2c_sfp_init();
	check_and_set_led = 2;

	/* ...and their values are updated */
	wr_zen_sfp_i2c_update(1);

#endif

	/*check if EEPROM is onboard*/
	eeprom_present(WRPC_FMC_I2C, FMC_EEPROM_ADR);

#ifdef CONFIG_LENTRIG

	/* New WR-LEN connector outputs initialization
	 *  There are two modules involved;
	 *    · The LEN Trigger module
	 *    · The GPIO module.
	 *  */
	wr_len_connector_init();
	pp_printf("------------------------\n");
#endif

#ifdef CONFIG_IRIGB
	/* Irig-B initialization */
		irigb_init();

		pp_printf("------------------------\n");
#endif

	mac_addr[0][0] = 0x08;	//
	mac_addr[0][1] = 0x00;	// CERN OUI
	mac_addr[0][2] = 0x30;	//

	if (get_persistent_mac(ONEWIRE_PORT, mac_addr[0]) == -1) {
		mprintf("Unable to determine MAC addresses\n");
		//PORT 1
		mac_addr[0][0] = 0x11;	//
		mac_addr[0][1] = 0x22;	//
		mac_addr[0][2] = 0x33;	// fallback MAC if get_persistent_mac fails
		mac_addr[0][3] = 0x44;	//
		mac_addr[0][4] = 0x55;	//
		mac_addr[0][5] = 0x66;	//

		//PORT 2
		mac_addr[1][0] = 0x11;	//
		mac_addr[1][1] = 0x22;	//
		mac_addr[1][2] = 0x33;	// fallback MAC if get_persistent_mac fails
		mac_addr[1][3] = 0x44;	//
		mac_addr[1][4] = 0x55;	//
		mac_addr[1][5] = 0x66;	//
	}else{

		memcpy(mac_addr[1], mac_addr[0], 6);
		mac_addr[1][5]++;
	}

	/* MAC addresses */
	pp_printf("Local MAC address port[0] (Port 1) is:	%x:%x:%x:%x:%x:%x\n", mac_addr[0][0],
			mac_addr[0][1], mac_addr[0][2], mac_addr[0][3], mac_addr[0][4], mac_addr[0][5]);

	pp_printf("Local MAC address port[1] (Port 2) is:	%x:%x:%x:%x:%x:%x\n", mac_addr[1][0],
				mac_addr[1][1], mac_addr[1][2], mac_addr[1][3], mac_addr[1][4], mac_addr[1][5]);

	pp_printf("------------------------\n");


	//Duplicate the configuration for both ports.
	for (i = 0; i < wrlen_num_ports; i++){
		ep_init(mac_addr[i], i);
		ep_enable(1, 1, i,1);
		minic_init(i);

		pp_printf("------------------------\n");
	}

	shw_pps_gen_init();
	wrc_ptp_init();

	pp_printf("------------------------\n");


	wrc_init_stats();

	//try reading t24 phase transition from EEPROM
	calib_t24p(WRC_MODE_MASTER, &cal_phase_transition[0], 0);
	calib_t24p(WRC_MODE_MASTER, &cal_phase_transition[1], 1);

#ifdef CONFIG_ETHPORT
	pp_printf("------------------------\n");

	/* Ethernet subsystem module initialization */
	eth_subsystem_init();

	pp_printf("------------------------\n");

	/* Mini switch module is enabled. */
	ms_init();

	/*Read and print the ID from the from FRU information at the EEPROM */
	// eeprom_read_board_fru(WRPC_FMC_I2C, FMC_EEPROM_ADR);

#endif


#ifdef CONFIG_ETHERBONE
	ipv4_init("wr0");
	ipv4_init("wr1");

	arp_init("wr0"); //By now the ARP is enabled.
	arp_init("wr1"); //By now the ARP is enabled.
#endif

}

#define LINK_WENT_UP 1
#define LINK_WENT_DOWN 2
#define LINK_UP 3
#define LINK_DOWN 4

static int wrc_check_link(int port)
{
	static int prev_link_state[2]= { -1, -1};	//All the elements to 0. WR-LEN

	int link_state = ep_link_up(NULL, port);

	int rv = 0;

	if (!prev_link_state[port] && link_state) {
		TRACE_DEV("Link up.\n");
		gpio_out(GPIO_LED_LINK, 1);
		
		#ifdef CONFIG_ZEN_BOARD
			wr_zen_link_led(1,port);
		#endif
		
		rv = LINK_WENT_UP;
	} else if (prev_link_state[port] && !link_state) {
		TRACE_DEV("Link down.\n");
		gpio_out(GPIO_LED_LINK, 0);
		
		#ifdef CONFIG_ZEN_BOARD
			wr_zen_link_led(0,port);
		#endif
		
		rv = LINK_WENT_DOWN;
	} else
		rv = (link_state ? LINK_UP : LINK_DOWN);
		
		#ifdef CONFIG_ZEN_BOARD
			if(check_and_set_led > 0) {
				wr_zen_link_led((link_state ? 1 : 0),port);
				check_and_set_led--;
			}
		#endif

#ifdef CONFIG_ETHPORT
	if( rv == LINK_WENT_DOWN )
		ms_reset_rtu_ep(port);
#endif

	prev_link_state[port] = link_state;

	return rv;
}

void wrc_debug_printf(int subsys, const char *fmt, ...)
{
	va_list ap;

	if (wrc_ui_mode)
		return;

	va_start(ap, fmt);

	if (subsys & (1 << 5) /* was: TRACE_SERVO -- see commit message */)
		vprintf(fmt, ap);

	va_end(ap);
}

int wrc_man_phase = 0;

static void ui_update()
{

	wrc_dump_refresh(0);
	if (wrc_ui_mode == UI_GUI_MODE) {
		wrc_mon_gui();
		if (uart_read_byte() == 27) {
			shell_init();
			wrc_ui_mode = UI_SHELL_MODE;
		}
	} else if (wrc_ui_mode == UI_STAT_MODE) {
		wrc_log_stats(0);
		if (uart_read_byte() == 27) {
			shell_init();
			wrc_ui_mode = UI_SHELL_MODE;
		}
	} else
		shell_interactive();

}

extern uint32_t _endram;
extern uint32_t _fstack;
#define ENDRAM_MAGIC 0xbadc0ffe

static void check_stack(void)
{
	while (_endram != ENDRAM_MAGIC) {
		mprintf("Stack overflow!\n");
		timer_delay(1000);
	}
}

#ifdef CONFIG_CHECK_RESET

static void check_reset(void)
{
	extern void _reset_handler(void); /* user to reset again */
	/* static variables to preserve stack (for dumping it) */
	static uint32_t *p, *save;

	/* _endram is set to ENDRAM_MAGIC after calling this function */
	if (_endram != ENDRAM_MAGIC)
		return;

	/* Before calling anything, find the beginning of the stack */
	p = &_endram + 1;
	while (!*p)
		p++;
	p = (void *)((unsigned long)p & ~0xf); /* align */

	/* Copy it to the beginning of the stack, then reset pointers */
	save = &_endram;
	while (p <= &_fstack)
		*save++ = *p++;
	p -= (save - &_endram);
	save = &_endram;

	/* Ok, now init the devices so we can printf and delay */
	sdb_find_devices();
	uart_init_sw();
	uart_init_hw();
	timer_init(1);

	pp_printf("\nWarning: the CPU was reset\nStack trace:\n");
	while (p < &_fstack) {
		pp_printf("%08x: %08x %08x %08x %08x\n",
			  (int)p, save[0], save[1], save[2], save[3]);
		p += 4;
		save += 4;
	}
	pp_printf("Rebooting in 1 second\n\n\n");
	timer_delay(1000);

	/* Zero the stack and start over (so we dump correctly next time) */
	for (p = &_endram; p < &_fstack; p++)
		*p = 0;
	_endram = 0;
	_reset_handler();
}

# else /* no CONFIG_CHECK_RESET */

static void check_reset(void) {}

#endif


int main(void)
{
	int i;
	int l_status[wrlen_num_ports];

	check_reset();
	wrc_ui_mode = UI_SHELL_MODE;
	_endram = ENDRAM_MAGIC;

	wr_board_name_initialize();
	wrc_initialize();

	pp_printf("------------------------\n");
	usleep_init();

	pp_printf("------------------------\n");
	shell_init();

	wrc_ptp_set_mode(WRC_SLAVE_WR0);	//The sfp port is slave by default.  WRC_MODE_GM
	wrc_ptp_start();
	pp_printf("------------------------\n");

	// The PTP must be updated once before the init script. The link detection could interfere with
	// the init commands; i.e. the triggers
	wrc_ptp_update();
	pp_printf("------------------------\n");

	//try to read and execute init script from EEPROM
	shell_boot_script();
	pp_printf("------------------------\n");

#if defined(CONFIG_ZEN_BOARD) || defined(CONFIG_ZEN_CTA_BOARD)
	/*
	 * Reseting the GTP to avoid the chicken and the egg situation.
	 * The GTP needs the dtp_dedicated_clock to start to work but when the
	 * FPGA is programmed resets the AD9516...
	 * So each time the LM32 is reset by the ARM, the GTP is reset as well.
	 */
	wr_zen_gpio_gtp_rst();
#endif

	for (;;) {

		//One iteration per each port
		for(i=0; i < wrlen_num_ports ;i++)
		{

			l_status[i] = wrc_check_link(i); //Check link status
			switch (l_status[i]) {

#ifdef CONFIG_ETHERBONE
			case LINK_WENT_UP:
				if (l_status[(i+1)%2])
					needIP = 1;	//BOOTP enabled
			break;
#endif

			case LINK_UP:
				update_rx_queues(i);

#ifdef CONFIG_ETHERBONE
				ipv4_poll(i);	//Check if we have received some icmp packets.
				arp_poll(i);	//Now the arp is enabled.
#endif
				break;
			}

			// Checking the SPF case for loading the deltas.
			sfp_detect_and_match(i);
		}

		ui_update();
		spll_update();
		wrc_ptp_update();
		spll_update_aux_clocks();
#ifdef CONFIG_ZEN_BOARD
		wr_zen_sfp_i2c_update(0);
#endif
#ifdef CONFIG_ETHPORT
		ms_eth_update();
#endif
		check_stack();
	}
}
