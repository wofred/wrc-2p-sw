################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../pp_printf/printf.o \
../pp_printf/vsprintf-mini.o \
../pp_printf/vsprintf-none.o \
../pp_printf/vsprintf-xint.o 

C_SRCS += \
../pp_printf/example-printf.c \
../pp_printf/printf.c \
../pp_printf/vsprintf-full.c \
../pp_printf/vsprintf-mini.c \
../pp_printf/vsprintf-none.c \
../pp_printf/vsprintf-xint.c 

OBJS += \
./pp_printf/example-printf.o \
./pp_printf/printf.o \
./pp_printf/vsprintf-full.o \
./pp_printf/vsprintf-mini.o \
./pp_printf/vsprintf-none.o \
./pp_printf/vsprintf-xint.o 

C_DEPS += \
./pp_printf/example-printf.d \
./pp_printf/printf.d \
./pp_printf/vsprintf-full.d \
./pp_printf/vsprintf-mini.d \
./pp_printf/vsprintf-none.d \
./pp_printf/vsprintf-xint.d 


# Each subdirectory must supply rules for building sources it contributes
pp_printf/%.o: ../pp_printf/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


