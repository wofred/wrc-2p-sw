################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../tools/eb-w1-write.c \
../tools/eb-w1.c \
../tools/genraminit.c \
../tools/genrammif.c \
../tools/genramvhd.c \
../tools/wrpc-uart-sw.c \
../tools/wrpc-w1-read.c \
../tools/wrpc-w1-write.c 

OBJS += \
./tools/eb-w1-write.o \
./tools/eb-w1.o \
./tools/genraminit.o \
./tools/genrammif.o \
./tools/genramvhd.o \
./tools/wrpc-uart-sw.o \
./tools/wrpc-w1-read.o \
./tools/wrpc-w1-write.o 

C_DEPS += \
./tools/eb-w1-write.d \
./tools/eb-w1.d \
./tools/genraminit.d \
./tools/genrammif.d \
./tools/genramvhd.d \
./tools/wrpc-uart-sw.d \
./tools/wrpc-w1-read.d \
./tools/wrpc-w1-write.d 


# Each subdirectory must supply rules for building sources it contributes
tools/%.o: ../tools/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


