################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../check-error.o \
../config.o \
../revision.o \
../wrc.o \
../wrc_main.o \
../wrc_ptp_noposix.o 

C_SRCS += \
../check-error.c \
../revision.c \
../wrc_main.c \
../wrc_ptp_noposix.c 

S_UPPER_SRCS += \
../wrc_disasm.S 

OBJS += \
./check-error.o \
./revision.o \
./wrc_disasm.o \
./wrc_main.o \
./wrc_ptp_noposix.o 

C_DEPS += \
./check-error.d \
./revision.d \
./wrc_main.d \
./wrc_ptp_noposix.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

%.o: ../%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Assembler'
	as  -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


