################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../ptp-noposix/PTPWRd/arith.o \
../ptp-noposix/PTPWRd/bmc.o \
../ptp-noposix/PTPWRd/protocol.o \
../ptp-noposix/PTPWRd/wr_protocol.o 

C_SRCS += \
../ptp-noposix/PTPWRd/arith.c \
../ptp-noposix/PTPWRd/bmc.c \
../ptp-noposix/PTPWRd/display.c \
../ptp-noposix/PTPWRd/protocol.c \
../ptp-noposix/PTPWRd/ptpd.c \
../ptp-noposix/PTPWRd/ptpd_exports.c \
../ptp-noposix/PTPWRd/wr_protocol.c 

OBJS += \
./ptp-noposix/PTPWRd/arith.o \
./ptp-noposix/PTPWRd/bmc.o \
./ptp-noposix/PTPWRd/display.o \
./ptp-noposix/PTPWRd/protocol.o \
./ptp-noposix/PTPWRd/ptpd.o \
./ptp-noposix/PTPWRd/ptpd_exports.o \
./ptp-noposix/PTPWRd/wr_protocol.o 

C_DEPS += \
./ptp-noposix/PTPWRd/arith.d \
./ptp-noposix/PTPWRd/bmc.d \
./ptp-noposix/PTPWRd/display.d \
./ptp-noposix/PTPWRd/protocol.d \
./ptp-noposix/PTPWRd/ptpd.d \
./ptp-noposix/PTPWRd/ptpd_exports.d \
./ptp-noposix/PTPWRd/wr_protocol.d 


# Each subdirectory must supply rules for building sources it contributes
ptp-noposix/PTPWRd/%.o: ../ptp-noposix/PTPWRd/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


