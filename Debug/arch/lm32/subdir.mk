################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../arch/lm32/crt0.o \
../arch/lm32/debug.o \
../arch/lm32/irq.o 

C_SRCS += \
../arch/lm32/irq.c 

S_UPPER_SRCS += \
../arch/lm32/crt0.S \
../arch/lm32/debug.S \
../arch/lm32/ram.ld.S 

OBJS += \
./arch/lm32/crt0.o \
./arch/lm32/debug.o \
./arch/lm32/irq.o \
./arch/lm32/ram.ld.o 

C_DEPS += \
./arch/lm32/irq.d 


# Each subdirectory must supply rules for building sources it contributes
arch/lm32/%.o: ../arch/lm32/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Assembler'
	as  -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

arch/lm32/%.o: ../arch/lm32/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


