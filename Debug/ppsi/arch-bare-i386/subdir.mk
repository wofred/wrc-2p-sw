################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ppsi/arch-bare-i386/syscalls.c 

S_UPPER_SRCS += \
../ppsi/arch-bare-i386/crt0.S 

OBJS += \
./ppsi/arch-bare-i386/crt0.o \
./ppsi/arch-bare-i386/syscalls.o 

C_DEPS += \
./ppsi/arch-bare-i386/syscalls.d 


# Each subdirectory must supply rules for building sources it contributes
ppsi/arch-bare-i386/%.o: ../ppsi/arch-bare-i386/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Assembler'
	as  -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

ppsi/arch-bare-i386/%.o: ../ppsi/arch-bare-i386/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


