################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ppsi/tools/mtp/mtp_packet.c \
../ppsi/tools/mtp/mtp_stamp.c \
../ppsi/tools/mtp/mtp_udp.c \
../ppsi/tools/mtp/onestamp.c \
../ppsi/tools/mtp/report.c \
../ppsi/tools/mtp/stamp-funcs.c \
../ppsi/tools/mtp/ttstamp.c 

OBJS += \
./ppsi/tools/mtp/mtp_packet.o \
./ppsi/tools/mtp/mtp_stamp.o \
./ppsi/tools/mtp/mtp_udp.o \
./ppsi/tools/mtp/onestamp.o \
./ppsi/tools/mtp/report.o \
./ppsi/tools/mtp/stamp-funcs.o \
./ppsi/tools/mtp/ttstamp.o 

C_DEPS += \
./ppsi/tools/mtp/mtp_packet.d \
./ppsi/tools/mtp/mtp_stamp.d \
./ppsi/tools/mtp/mtp_udp.d \
./ppsi/tools/mtp/onestamp.d \
./ppsi/tools/mtp/report.d \
./ppsi/tools/mtp/stamp-funcs.d \
./ppsi/tools/mtp/ttstamp.d 


# Each subdirectory must supply rules for building sources it contributes
ppsi/tools/mtp/%.o: ../ppsi/tools/mtp/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


