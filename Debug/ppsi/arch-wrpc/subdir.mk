################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../ppsi/arch-wrpc/wrc_ptp_ppsi.o \
../ppsi/arch-wrpc/wrpc-calibration.o \
../ppsi/arch-wrpc/wrpc-io.o \
../ppsi/arch-wrpc/wrpc-spll.o 

C_SRCS += \
../ppsi/arch-wrpc/wrc_ptp_ppsi.c \
../ppsi/arch-wrpc/wrpc-calibration.c \
../ppsi/arch-wrpc/wrpc-io.c \
../ppsi/arch-wrpc/wrpc-spll.c 

OBJS += \
./ppsi/arch-wrpc/wrc_ptp_ppsi.o \
./ppsi/arch-wrpc/wrpc-calibration.o \
./ppsi/arch-wrpc/wrpc-io.o \
./ppsi/arch-wrpc/wrpc-spll.o 

C_DEPS += \
./ppsi/arch-wrpc/wrc_ptp_ppsi.d \
./ppsi/arch-wrpc/wrpc-calibration.d \
./ppsi/arch-wrpc/wrpc-io.d \
./ppsi/arch-wrpc/wrpc-spll.d 


# Each subdirectory must supply rules for building sources it contributes
ppsi/arch-wrpc/%.o: ../ppsi/arch-wrpc/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


