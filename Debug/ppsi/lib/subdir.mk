################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../ppsi/lib/div64.o \
../ppsi/lib/dump-funcs.o 

C_SRCS += \
../ppsi/lib/cmdline.c \
../ppsi/lib/conf.c \
../ppsi/lib/div64.c \
../ppsi/lib/dump-funcs.c \
../ppsi/lib/libc-functions.c 

OBJS += \
./ppsi/lib/cmdline.o \
./ppsi/lib/conf.o \
./ppsi/lib/div64.o \
./ppsi/lib/dump-funcs.o \
./ppsi/lib/libc-functions.o 

C_DEPS += \
./ppsi/lib/cmdline.d \
./ppsi/lib/conf.d \
./ppsi/lib/div64.d \
./ppsi/lib/dump-funcs.d \
./ppsi/lib/libc-functions.d 


# Each subdirectory must supply rules for building sources it contributes
ppsi/lib/%.o: ../ppsi/lib/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


