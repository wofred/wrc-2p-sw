################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ppsi/arch-wrs/main-loop.c \
../ppsi/arch-wrs/wrs-calibration.c \
../ppsi/arch-wrs/wrs-io.c \
../ppsi/arch-wrs/wrs-ipcserver.c \
../ppsi/arch-wrs/wrs-startup.c 

OBJS += \
./ppsi/arch-wrs/main-loop.o \
./ppsi/arch-wrs/wrs-calibration.o \
./ppsi/arch-wrs/wrs-io.o \
./ppsi/arch-wrs/wrs-ipcserver.o \
./ppsi/arch-wrs/wrs-startup.o 

C_DEPS += \
./ppsi/arch-wrs/main-loop.d \
./ppsi/arch-wrs/wrs-calibration.d \
./ppsi/arch-wrs/wrs-io.d \
./ppsi/arch-wrs/wrs-ipcserver.d \
./ppsi/arch-wrs/wrs-startup.d 


# Each subdirectory must supply rules for building sources it contributes
ppsi/arch-wrs/%.o: ../ppsi/arch-wrs/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


