################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ppsi/arch-bare-x86-64/syscalls.c 

S_UPPER_SRCS += \
../ppsi/arch-bare-x86-64/crt0.S \
../ppsi/arch-bare-x86-64/syscall.S 

OBJS += \
./ppsi/arch-bare-x86-64/crt0.o \
./ppsi/arch-bare-x86-64/syscall.o \
./ppsi/arch-bare-x86-64/syscalls.o 

C_DEPS += \
./ppsi/arch-bare-x86-64/syscalls.d 


# Each subdirectory must supply rules for building sources it contributes
ppsi/arch-bare-x86-64/%.o: ../ppsi/arch-bare-x86-64/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Assembler'
	as  -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

ppsi/arch-bare-x86-64/%.o: ../ppsi/arch-bare-x86-64/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


