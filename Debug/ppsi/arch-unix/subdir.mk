################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ppsi/arch-unix/main-loop.c \
../ppsi/arch-unix/unix-io.c \
../ppsi/arch-unix/unix-startup.c 

OBJS += \
./ppsi/arch-unix/main-loop.o \
./ppsi/arch-unix/unix-io.o \
./ppsi/arch-unix/unix-startup.o 

C_DEPS += \
./ppsi/arch-unix/main-loop.d \
./ppsi/arch-unix/unix-io.d \
./ppsi/arch-unix/unix-startup.d 


# Each subdirectory must supply rules for building sources it contributes
ppsi/arch-unix/%.o: ../ppsi/arch-unix/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


