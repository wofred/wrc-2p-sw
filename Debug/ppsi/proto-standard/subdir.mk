################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../ppsi/proto-standard/arith.o \
../ppsi/proto-standard/bmc.o \
../ppsi/proto-standard/common-fun.o \
../ppsi/proto-standard/fsm-table.o \
../ppsi/proto-standard/hooks.o \
../ppsi/proto-standard/msg.o \
../ppsi/proto-standard/open-close.o \
../ppsi/proto-standard/servo.o \
../ppsi/proto-standard/state-disabled.o \
../ppsi/proto-standard/state-faulty.o \
../ppsi/proto-standard/state-initializing.o \
../ppsi/proto-standard/state-listening.o \
../ppsi/proto-standard/state-master.o \
../ppsi/proto-standard/state-passive.o \
../ppsi/proto-standard/state-pre-master.o \
../ppsi/proto-standard/state-slave.o \
../ppsi/proto-standard/state-uncalibrated.o \
../ppsi/proto-standard/timeout.o 

C_SRCS += \
../ppsi/proto-standard/arith.c \
../ppsi/proto-standard/bmc.c \
../ppsi/proto-standard/common-fun.c \
../ppsi/proto-standard/fsm-table.c \
../ppsi/proto-standard/hooks.c \
../ppsi/proto-standard/msg.c \
../ppsi/proto-standard/open-close.c \
../ppsi/proto-standard/servo.c \
../ppsi/proto-standard/state-disabled.c \
../ppsi/proto-standard/state-faulty.c \
../ppsi/proto-standard/state-initializing.c \
../ppsi/proto-standard/state-listening.c \
../ppsi/proto-standard/state-master.c \
../ppsi/proto-standard/state-passive.c \
../ppsi/proto-standard/state-pre-master.c \
../ppsi/proto-standard/state-slave.c \
../ppsi/proto-standard/state-uncalibrated.c \
../ppsi/proto-standard/timeout.c 

OBJS += \
./ppsi/proto-standard/arith.o \
./ppsi/proto-standard/bmc.o \
./ppsi/proto-standard/common-fun.o \
./ppsi/proto-standard/fsm-table.o \
./ppsi/proto-standard/hooks.o \
./ppsi/proto-standard/msg.o \
./ppsi/proto-standard/open-close.o \
./ppsi/proto-standard/servo.o \
./ppsi/proto-standard/state-disabled.o \
./ppsi/proto-standard/state-faulty.o \
./ppsi/proto-standard/state-initializing.o \
./ppsi/proto-standard/state-listening.o \
./ppsi/proto-standard/state-master.o \
./ppsi/proto-standard/state-passive.o \
./ppsi/proto-standard/state-pre-master.o \
./ppsi/proto-standard/state-slave.o \
./ppsi/proto-standard/state-uncalibrated.o \
./ppsi/proto-standard/timeout.o 

C_DEPS += \
./ppsi/proto-standard/arith.d \
./ppsi/proto-standard/bmc.d \
./ppsi/proto-standard/common-fun.d \
./ppsi/proto-standard/fsm-table.d \
./ppsi/proto-standard/hooks.d \
./ppsi/proto-standard/msg.d \
./ppsi/proto-standard/open-close.d \
./ppsi/proto-standard/servo.d \
./ppsi/proto-standard/state-disabled.d \
./ppsi/proto-standard/state-faulty.d \
./ppsi/proto-standard/state-initializing.d \
./ppsi/proto-standard/state-listening.d \
./ppsi/proto-standard/state-master.d \
./ppsi/proto-standard/state-passive.d \
./ppsi/proto-standard/state-pre-master.d \
./ppsi/proto-standard/state-slave.d \
./ppsi/proto-standard/state-uncalibrated.d \
./ppsi/proto-standard/timeout.d 


# Each subdirectory must supply rules for building sources it contributes
ppsi/proto-standard/%.o: ../ppsi/proto-standard/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


