################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../shell/cmd_calib.o \
../shell/cmd_gui.o \
../shell/cmd_help.o \
../shell/cmd_init.o \
../shell/cmd_ip.o \
../shell/cmd_mac.o \
../shell/cmd_mode.o \
../shell/cmd_pll.o \
../shell/cmd_ptp.o \
../shell/cmd_ptrack.o \
../shell/cmd_sdb.o \
../shell/cmd_sfp.o \
../shell/cmd_stat.o \
../shell/cmd_time.o \
../shell/cmd_verbose.o \
../shell/cmd_version.o \
../shell/cmd_wb.o \
../shell/shell.o 

C_SRCS += \
../shell/cmd_calib.c \
../shell/cmd_config.c \
../shell/cmd_gui.c \
../shell/cmd_help.c \
../shell/cmd_init.c \
../shell/cmd_ip.c \
../shell/cmd_mac.c \
../shell/cmd_mode.c \
../shell/cmd_pll.c \
../shell/cmd_ptp.c \
../shell/cmd_ptrack.c \
../shell/cmd_sdb.c \
../shell/cmd_sfp.c \
../shell/cmd_sleep.c \
../shell/cmd_stat.c \
../shell/cmd_time.c \
../shell/cmd_verbose.c \
../shell/cmd_version.c \
../shell/cmd_wb.c \
../shell/shell.c 

OBJS += \
./shell/cmd_calib.o \
./shell/cmd_config.o \
./shell/cmd_gui.o \
./shell/cmd_help.o \
./shell/cmd_init.o \
./shell/cmd_ip.o \
./shell/cmd_mac.o \
./shell/cmd_mode.o \
./shell/cmd_pll.o \
./shell/cmd_ptp.o \
./shell/cmd_ptrack.o \
./shell/cmd_sdb.o \
./shell/cmd_sfp.o \
./shell/cmd_sleep.o \
./shell/cmd_stat.o \
./shell/cmd_time.o \
./shell/cmd_verbose.o \
./shell/cmd_version.o \
./shell/cmd_wb.o \
./shell/shell.o 

C_DEPS += \
./shell/cmd_calib.d \
./shell/cmd_config.d \
./shell/cmd_gui.d \
./shell/cmd_help.d \
./shell/cmd_init.d \
./shell/cmd_ip.d \
./shell/cmd_mac.d \
./shell/cmd_mode.d \
./shell/cmd_pll.d \
./shell/cmd_ptp.d \
./shell/cmd_ptrack.d \
./shell/cmd_sdb.d \
./shell/cmd_sfp.d \
./shell/cmd_sleep.d \
./shell/cmd_stat.d \
./shell/cmd_time.d \
./shell/cmd_verbose.d \
./shell/cmd_version.d \
./shell/cmd_wb.d \
./shell/shell.d 


# Each subdirectory must supply rules for building sources it contributes
shell/%.o: ../shell/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


