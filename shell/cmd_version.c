#include <wrc.h>
#include "shell.h"
#include "syscon.h"
#include "eeprom.h"
#include "gw_version.h"

extern const char *build_revision, *build_date;

static int cmd_ver(const char *args[])
{
	int hwram = sysc_get_memsize();

	pp_printf("WR Core build: %s\n", build_revision);
	pp_printf("%s", build_date); /* may be empty, or complete with \n */
	pp_printf("Built for %d kB RAM, stack is %d bytes\n",
		  CONFIG_RAMSIZE / 1024, CONFIG_STACKSIZE);
	/* hardware reports memory size, with a 16kB granularity */
	if ( hwram / 16 != CONFIG_RAMSIZE / 1024 / 16)
		pp_printf("WARNING: hardware says %ikB <= RAM < %ikB\n",
			  hwram, hwram + 16);

	#ifdef CONFIG_LEN_BOARD
		/*Print the 1-wire rom id (DS18B20U) */
		pp_printf("\n1-wire ROM ID (DS18B20U) : 0x%16x \n", w1_sensor_rom);

		/*Read and print the ID from the from FRU information at the EEPROM */
		eeprom_read_board_fru(WRPC_FMC_I2C, FMC_EEPROM_ADR);
	#endif

	/* It prints the GW info */
	print_gw_info();
	return 0;
}

DEFINE_WRC_COMMAND(ver) = {
	.name = "ver",
	.exec = cmd_ver,
};
