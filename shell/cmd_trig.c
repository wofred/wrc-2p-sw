/*
 * This work is part of the WR-LEN project
 *
 * Authors:
 * Pablo Marín <marin<at>sevensols.com>
 * Emilio Marín <emilio<at>sevensols.com>
 *
 * Command to program triggers from the DIO module
*/

#include <stdio.h>
#include "shell.h"
#include <wrpc.h>
#include <string.h>
#include "types.h"
#include "board.h"
#include "util.h"

#include "wr-dio.h" //DIO module
#include "hw/wrsw_dio_wb.h"

#ifdef CONFIG_LENTRIG
  #include "len_fine_tune.h"
  #include "eeprom.h"
  #include "syscon.h"
#endif

volatile struct wr_dio_cmd _cmd;
volatile struct wr_dio_cmd *cmd = &_cmd;

uint8_t pps_clk_output_ena  = 0;

uint8_t ch_min = 0;
uint8_t ch_max = 4;

/* Temporally function to decode the hex value for the PCA9554 */
static void decode_hex(const char *str, uint8_t *value)
{
  unsigned long long_value= strtoul(str, NULL, 0);
  *value = (uint8_t)long_value;
};

 /*
  * void print_cmd(){
  *
  * mprintf("command = %i; channel = %i; pulse = %i; flags = %i; time_posix = %i; tm_ns = %i \n", cmd->command, cmd->channel,
  *    cmd->pulse, cmd->flags, cmd->time_posix, cmd->tm_ns);
  * }
  */

static int cmd_trig(const char *args[])
{
  int ch_n = -1;
  //¿int l_sec = 10;?
  uint32_t l_sec = 0;
  uint32_t l_nsec = 0;
  uint8_t arg_indx = 0;
  uint8_t pca_out = 0;

  uint8_t pps_fine = 0;
  uint8_t clk_fine = 0;
  uint8_t clk_coarse = 0;
  uint32_t pps_coarse;

  uint8_t clk_period = REF_CLOCK_PERIOD_PS/1000;

  uint64_t utc_sec;
  uint32_t utc_nsec;

  shw_pps_gen_get_time(&utc_sec, &utc_nsec);

  /*
  * Parse the command line:
  *
  * trig <ch> <len> now
  * trig <ch> <len> -t <s> [ns] [-l <s> [ns]]
  * trig <len> -t <s> [ns] –r <ns1> <ns2> <ns3> <ns4>
  */

#ifdef CONFIG_LENTRIG

    /* Here is the case for enabling/disabling the PPS & CLK outputs */
    if (!strcasecmp(args[arg_indx], "pps_clk") || (pps_clk_output_ena == 1) ){
      arg_indx++;

      if (!strcasecmp(args[arg_indx], "off")){
        pps_clk_output_ena = 0;
        wr_len_pps_and_clk_ena(0);
      } else if (!strcasecmp(args[arg_indx], "on")) {
        pps_clk_output_ena = 1;
        wr_len_pps_and_clk_ena(1);

      } else if (!strcasecmp(args[arg_indx], "-c")) {
        arg_indx++;

        if (!strcasecmp(args[arg_indx], "-pps")) {
          arg_indx++;

	  // PPS Offset
	  pps_coarse =  atoi(args[arg_indx]);
	  pps_coarse = pps_coarse - pps_coarse%clk_period;
	  arg_indx++;

          // PPS Fine Tune.
          decode_hex(args[arg_indx], &pps_fine);
          arg_indx++;

          if (!strcasecmp(args[arg_indx], "-clk")) {
            arg_indx++;

            // CLK shifts.
            clk_coarse = (atoi(args[arg_indx]) > 0xF ) ? 0xF : atoi(args[arg_indx]);
            arg_indx++;

            // CLK Fine tune.
            decode_hex(args[arg_indx], &clk_fine);
            arg_indx++;

            }
          }

        /* Store the CLK and PPS parameters in the EEPROM or don't... */
	if (!strcasecmp(args[arg_indx], "save")) {
	    arg_indx++;

	    /* The parameters are saved & stored in the EEPROM for future uses */
	    wr_len_save_parameters(pps_coarse, pps_fine, clk_coarse, clk_fine, 1);
	} else/* Save the parameters...*/
	  wr_len_save_parameters(pps_coarse, pps_fine, clk_coarse, clk_fine, 0);

	 /* ... and load them in case */
	if (pps_clk_output_ena == 1)
	  wr_len_pps_and_clk_ena(1);

      } else {
	  mprintf(" >trig \n");
	  mprintf("       pps_clk <on/off> \n");
	  mprintf("       pps_clk  -c -pps <coarse_tune> <0xfine_tune> -clk <coarse_tune> <0xfine_tune> \n\n");
	}

      mprintf("PPS & CLK DB9 status = %i \n", pps_clk_output_ena );
      return 0;
    } /* End of the pps_clk case */

#endif

    // Checking valid channel number
    ch_n = atoi(args[arg_indx]);
    arg_indx++;

    // Checking the the reset option
    if (!strcasecmp(args[arg_indx], "reset")) {

	if (strcasecmp(args[arg_indx-1], "all") == 0) {
	  dio_reset(0x1f);
	  mprintf("All triggers have been disabled. \n");
	} else if ((ch_n <= ch_max && ch_n >= ch_min)){
	  dio_reset(1 << ch_n);
	  mprintf("Trigger %i disabled. \n", ch_n);
	}
	return 0;
    }

    if (ch_n > ch_max || ch_n < ch_min ||  (strcasecmp(args[arg_indx-1], "0") != 0) && ch_n == 0) {
	mprintf(" Not a valid channel number [%i-%i].\n", ch_min, ch_max);
	return 0;
    } else
      cmd->channel = ch_n;

    //Parsing trigger width
    cmd->pulse = parse_nanos(args[arg_indx]);
    arg_indx++;

    if (!strcasecmp(args[arg_indx], "now")) {  //Immediate trigger option
      arg_indx++;
      cmd->flags = WR_DIO_F_NOW;
      mprintf("  Channel: %i\n  Width: %ins\n  Time: NOW!\n", cmd->channel, (cmd->pulse - cmd->pulse%clk_period));

    } else if (!strcasecmp(args[arg_indx], "-t")) { //Time-specific trigger option
      arg_indx++;

      cmd->flags = WR_DIO_F_ABS;
      cmd->time_posix =  parse_64bits_args(args[arg_indx]);
      arg_indx++;

      //mprintf(" period = %i -> (hex) = 0x%16x : args[%i] = %s \n", (uint32_t)cmd->time_posix, cmd->time_posix, (arg_indx-1), args[arg_indx-1]);

      if( cmd->time_posix < utc_sec) {
	  mprintf(" Error, the current utc time (");
	  print_longer_than_32_bits(utc_sec);
	  mprintf("'') is higher than the initial trigger time ");
	  print_longer_than_32_bits(cmd->time_posix);
	  mprintf("'') \n");
	  return 0;
      }

      if (!strcasecmp(args[arg_indx], "-l")) {   //Trigger specific (seconds) + loop (seconds & nanoseconds)
        arg_indx++;

        cmd->flags = WR_DIO_F_LOOP;

        l_sec = atoi(args[arg_indx]);
        arg_indx++;
        l_nsec = atoi(args[arg_indx]);

        // In case l_nsec doesn't match the nanoseconds... it could be another argument.
        if( (l_nsec == 0) && (atoi(args[arg_indx]) != 0))
          arg_indx++;

        //Nanosecond periodic part must be equal or bigger than 96 ns
        if ((l_nsec != 0) && (l_nsec < 96)){
            mprintf("Error: nanosecond periodic option must be larger than 96 ns\n");
            return 0;
        }

        //Trigger specific nanoseconds to zero.
        cmd->tm_ns = 0;

        mprintf("  Channel: %i\n  Width: %ins\n  Time: ", cmd->channel, (cmd->pulse - cmd->pulse%clk_period));
        print_longer_than_32_bits(cmd->time_posix);
        mprintf("s %ins\n  Period: %is %ins\n", (cmd->tm_ns - cmd->tm_ns%clk_period) , l_sec, (l_nsec - l_nsec%clk_period));
      } else {  //Trigger specific (seconds & nanoseconds)

        cmd->tm_ns = parse_nanos(args[arg_indx]);

        // In case args[arg_indx] doesn't match the nanoseconds... it could be another argument.
        //if( (cmd->tm_ns == 0) && (atoi(args[arg_indx]) != 0))
          arg_indx++;

        if (!strcasecmp(args[arg_indx], "-l")) { //Trigger specific (seconds & nanoseconds) + loop (seconds + nanoseconds)
          arg_indx++;

          cmd->flags = WR_DIO_F_LOOP;

          l_sec = atoi(args[arg_indx]);
          arg_indx++;
          l_nsec = atoi(args[arg_indx]);

          // In case l_nsec doesn't match the nanoseconds... it could be another argument.
          if( (l_nsec == 0) && (atoi(args[arg_indx]) != 0))
            arg_indx++;

          //Nanosecond periodic part must be equal or bigger than 96 ns
          if ((l_nsec != 0) && (l_nsec < 96)){
              mprintf("Error: nanosecond periodic option must be larger than 96 ns\n");
              return 0;
          }

          mprintf("  Channel: %i\n  Width: %ins\n  Time: ", cmd->channel, (cmd->pulse - cmd->pulse%clk_period));
          print_longer_than_32_bits(cmd->time_posix);
          mprintf("s %ins\n  Period: %is %ins\n", (cmd->tm_ns - cmd->tm_ns%clk_period) , l_sec, (l_nsec - l_nsec%clk_period));

        } else {
          mprintf("  Channel: %i\n  Width: %ins\n  Time: ", cmd->channel, (cmd->pulse - cmd->pulse%clk_period));
          print_longer_than_32_bits(cmd->time_posix);
          mprintf("s %ins\n  Period: %is %ins\n", (cmd->tm_ns - cmd->tm_ns%clk_period) , l_sec, (l_nsec - l_nsec%clk_period));
        }
      }
    }

    if (wrn_dio_cmd_pulse(cmd,l_sec,l_nsec) < 0)
      mprintf(" Error pulsing\n");

  return 0;
}

DEFINE_WRC_COMMAND(trig) = {
  .name = "trig",
  .exec = cmd_trig,
};
