/*
 * This work is part of the White Rabbit project
 *
 * Copyright (C) 2012 GSI (www.gsi.de)
 * Author: Wesley W. Terpstra <w.terpstra@gsi.de>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 */
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <wrc.h>

#include "softpll_ng.h"
#include "shell.h"
#include "onewire.h"
#include "endpoint.h"
#include "../lib/ipv4.h"

static void decode_mac(const char *str, unsigned char *mac)
{
	int i, x;

	/* Don't try to detect bad input; need small code */
	for (i = 0; i < 6; ++i) {
		str = fromhex(str, &x);
		mac[i] = x;
		if (*str == ':')
			++str;
	}
}

//WR-LEN
static int cmd_mac(const char *args[])
{
	unsigned char mac[wrlen_num_ports][6];
	int port;

	if (!args[0] || !strcasecmp(args[0], "get")) {
		/* get current MAC */
		get_mac_addr(mac[0], 0);
		get_mac_addr(mac[1], 1);
	} else if (!strcasecmp(args[0], "getp")) {
		/* get persistent MAC */
		get_mac_addr(mac[0], 0);
		get_persistent_mac(ONEWIRE_PORT, mac[0]);
	} else if (!strcasecmp(args[0], "set") && args[1] && args[2]) {
		port = atoi(&args[1][2]) ;

		if(port < wrlen_num_ports){
			decode_mac(args[2], mac[port]);
			set_mac_addr(mac[port], port);
#ifdef CONFIG_LEN_BOARD
			pfilter_len_default(port);
#else
			pfilter_init_default(port);
#endif
		}

	} else if (!strcasecmp(args[0], "setp") && args[1]) {
		decode_mac(args[1], mac[0]);
		set_persistent_mac(ONEWIRE_PORT, mac);
	} else {
		return -EINVAL;
	}

	get_mac_addr(mac[0], 0);
	get_mac_addr(mac[1], 1);

	mprintf("MAC-address wr0 %02x:%02x:%02x:%02x:%02x:%02x\n",
		mac[0][0], mac[0][1], mac[0][2], mac[0][3], mac[0][4], mac[0][5]);
	mprintf("MAC-address wr1 %02x:%02x:%02x:%02x:%02x:%02x\n",
			mac[1][0], mac[1][1], mac[1][2], mac[1][3], mac[1][4], mac[1][5]);

	return 0;
}

DEFINE_WRC_COMMAND(mac) = {
	.name = "mac",
	.exec = cmd_mac,
};
