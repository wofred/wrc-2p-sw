obj-y += \
	shell/shell.o \
	shell/cmd_version.o \
	shell/cmd_pll.o \
	shell/cmd_sfp.o \
	shell/cmd_stat.o \
	shell/cmd_ptp.o \
	shell/cmd_mode.o \
	shell/cmd_calib.o \
	shell/cmd_time.o \
	shell/cmd_gui.o \
	shell/cmd_sdb.o \
	shell/cmd_mac.o \
	shell/cmd_init.o \
	shell/cmd_ptrack.o \
	shell/cmd_help.o \
  shell/cmd_wb.o

obj-$(CONFIG_DIO) +=				shell/cmd_trig.o
obj-$(CONFIG_ETHPORT) += 			shell/cmd_eth.o
obj-$(CONFIG_IRIGB) += 				shell/cmd_irigb.o
obj-$(CONFIG_ZEN_BOARD) +=			shell/cmd_i2csw.o
obj-$(CONFIG_ETHERBONE) +=			shell/cmd_ip.o
obj-$(CONFIG_PPSI) +=				shell/cmd_verbose.o
obj-$(CONFIG_CMD_CONFIG) +=			shell/cmd_config.o
obj-$(CONFIG_CMD_SLEEP) +=			shell/cmd_sleep.o
obj-$(CONFIG_EXPANSION_SPI) +=		shell/cmd_exp_spi.o
