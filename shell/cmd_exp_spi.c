/*
Command to read/write from/to the expansion SPI slave.
eml. emilio<AT>sevensols.com
*/

#include <stdlib.h>     /* strtol */
#include "shell.h"
#include <string.h>

#include "exp_spi.h"

static void decode_hex(const char *str, uint8_t *value)
{
	unsigned long long_value= strtoul(str, NULL, 0);
	*value = (uint8_t)long_value;
};

static int cmd_exp_spi(const char *args[])
{
	  uint8_t address;
	  uint8_t data;

	  if (!strcasecmp(args[0], "read") && args[1]) {
	      /* Read WB register */
	      decode_hex(args[1],&address);
	      data = exp_spi_read_reg(address);
	      mprintf("0x%08x \n", data);

		} else if (!strcasecmp(args[0], "write") && args[1] && args[2]) {
			/* Write WB resgister */
	      decode_hex(args[1], &address);
	      decode_hex(args[2], &data);
	      exp_spi_write_reg(address, data);

	   } else {
	     mprintf("Wrong exp_spi call! \n");
	     mprintf(">exp_spi read <0xregister> \n");
	     mprintf(">exp_spi write <0xregister> <0xdata>\n");
	   }

	  return 0;
};

DEFINE_WRC_COMMAND(exp_spi) = {
	.name = "exp_spi",
	.exec = cmd_exp_spi,
};
