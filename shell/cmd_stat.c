#include "shell.h"
#include <stdio.h>
#include "endpoint.h"
#include <string.h>
#include <wrc.h>

//WR-LEN
static int cmd_stat(const char *args[])
{
	int new_refresh_time = TICS_PER_SECOND;

	char *iface_port;
	int wr_port;

	if (!strcasecmp(args[0], "cont")) {
		wrc_ui_mode = UI_STAT_MODE;

		if(args[1]){
			new_refresh_time = atoi(args[1]);
				if ((new_refresh_time <= 10000) && (new_refresh_time >= 100)) {
				mprintf("The stats will be printed every %i ms. [ESC] to exit \n", new_refresh_time);
				ext_refresh_time = new_refresh_time;
			} else {
				mprintf("Error! milliseconds option out of range. It must be from 100 up to 10000. \n");
				mprintf("Refresh time to %i by default. [ESC] to exit \n", TICS_PER_SECOND);
			}

		}
	} else if (!strcasecmp(args[0], "bts")){
		iface_port = args[1];			//port name
		wr_port = atoi(&iface_port[2]);	//port index
		if (wr_port == 0 || wr_port == 1)
			mprintf("wr%i: %d ps\n", wr_port, ep_get_bitslide(wr_port));
		else
			mprintf("Error.\n");
	} else if (!strcasecmp(args[0], "help")){
		mprintf(">stat cont <refresh_time>       : To print in loop the stats. <refresh_time> in milliseconds \n");
		mprintf(">stat                           : Only one stat printed \n");
		mprintf(">stat bts <port>                : To print the bitslide at wr0 port. \n");
		mprintf(">stat help                      : To show this help\n");
	}
	else
		wrc_log_stats(1);

	return 0;
}

DEFINE_WRC_COMMAND(stat) = {
	.name = "stat",
	.exec = cmd_stat,
};
