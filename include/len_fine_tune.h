//#ifndef WR_LEN_I2C_H
//#define WR_LEN_I2C_H

#include <pca9554.h>
#include "wr-dio.h" //wr_dio_cmd struct.

#define GEN_OR_MASK(x) (1 << x)
#define GEN_AND_MASK(x) (~GEN_OR_MASK(x))

#define WR_LEN_CONN_I2C  3

#define N_FPGA_GPIO_BANK 32

#define WR_LEN_CONN_SCL 	      0x0
#define WR_LEN_CONN_SDA 	      0x1

#define N_GPIOS 2

#define FPGA_GPIO_CLR_ADDR(N) ((N/N_FPGA_GPIO_BANK)*0x20)
#define FPGA_GPIO_SET_ADDR(N) ((N/N_FPGA_GPIO_BANK)*0x20 + 0x4)
#define FPGA_GPIO_DIR_ADDR(N) ((N/N_FPGA_GPIO_BANK)*0x20 + 0x8)
#define FPGA_GPIO_STA_ADDR(N) ((N/N_FPGA_GPIO_BANK)*0x20 + 0xc)

#define FPGA_GPIO_SET_DATA(N) (0x1 << (N%N_FPGA_GPIO_BANK))
#define FPGA_GPIO_CLR_DATA(N) (FPGA_GPIO_SET_DATA(N))

#define PCA9554_I2C_DEFAULT_VALUE 0x00

#define PPS_OUTPUT_CH       0x0
#define DATA_CLK_OUTPUT_CH  0x1

#define PPS_TIME_GAP 0x1

void wr_len_gpio_set(int n_gpio);
void wr_len_gpio_clr(int n_gpio);
void wr_len_connector_init();
void wr_len_pps_and_clk_ena(uint8_t ena);
int wr_len_sync_pps_db9();
void wr_len_save_parameters(uint32_t pps_coarse, uint8_t pps_fine, uint8_t clk_coarse, uint8_t clk_fine, uint8_t save);

