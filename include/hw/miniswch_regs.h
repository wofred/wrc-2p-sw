/*
  Register definitions for slave core: Wishbone Mini Switch

  * File           : miniswch_regs.h
  * Author         : auto-generated by wbgen2 from miniswch.wb
  * Created        : Mon Jan 18 15:52:04 2016
  * Standard       : ANSI C

    THIS FILE WAS GENERATED BY wbgen2 FROM SOURCE FILE miniswch.wb
    DO NOT HAND-EDIT UNLESS IT'S ABSOLUTELY NECESSARY!

*/

#ifndef __WBGEN2_REGDEFS_MINISWCH_WB
#define __WBGEN2_REGDEFS_MINISWCH_WB

#include <inttypes.h>

#if defined( __GNUC__)
#define PACKED __attribute__ ((packed))
#else
#error "Unsupported compiler?"
#endif

#ifndef __WBGEN2_MACROS_DEFINED__
#define __WBGEN2_MACROS_DEFINED__
#define WBGEN2_GEN_MASK(offset, size) (((1ULL<<(size))-1) << (offset))
#define WBGEN2_GEN_WRITE(value, offset, size) (((value) & ((1<<(size))-1)) << (offset))
#define WBGEN2_GEN_READ(reg, offset, size) (((reg) >> (offset)) & ((1<<(size))-1))
#define WBGEN2_SIGN_EXTEND(value, bits) (((value) & (1<<bits) ? ~((1<<(bits))-1): 0 ) | (value))
#endif


/* definitions for register: Neighbour Host: MAC address high part register */

/* definitions for register: Neighbour Host: MAC address low part register */

/* definitions for register: Neighbour host control register */

/* definitions for field: Valid neighbour mac address. in reg: Neighbour host control register */
#define MINISWCH_NB_CRTL_NB_MAC_VALID         WBGEN2_GEN_MASK(0, 1)

/* definitions for field: Reset the MAC learning FSM. in reg: Neighbour host control register */
#define MINISWCH_NB_CRTL_NB_MAC_RST           WBGEN2_GEN_MASK(1, 1)

/* definitions for register: Register to manage the RTU fifo */

/* definitions for field: Filling FIFO request in reg: Register to manage the RTU fifo */
#define MINISWCH_MNG_RTU_FILL_REQ             WBGEN2_GEN_MASK(0, 1)

/* definitions for field: Reset EP0 RTU in reg: Register to manage the RTU fifo */
#define MINISWCH_MNG_RTU_RST_EP0              WBGEN2_GEN_MASK(1, 1)

/* definitions for field: Reset EP1 RTU in reg: Register to manage the RTU fifo */
#define MINISWCH_MNG_RTU_RST_EP1              WBGEN2_GEN_MASK(2, 1)

/* definitions for register: FIFO 'FIFO to retrieve the RTU info from the EP0' data output register 0 */

/* definitions for field: MAC address most-significant part in reg: FIFO 'FIFO to retrieve the RTU info from the EP0' data output register 0 */
#define MINISWCH_EP0_FIFO_R0_MAC_HI_MASK      WBGEN2_GEN_MASK(0, 16)
#define MINISWCH_EP0_FIFO_R0_MAC_HI_SHIFT     0
#define MINISWCH_EP0_FIFO_R0_MAC_HI_W(value)  WBGEN2_GEN_WRITE(value, 0, 16)
#define MINISWCH_EP0_FIFO_R0_MAC_HI_R(reg)    WBGEN2_GEN_READ(reg, 0, 16)

/* definitions for register: FIFO 'FIFO to retrieve the RTU info from the EP0' data output register 1 */

/* definitions for field: MAC address least-significant part in reg: FIFO 'FIFO to retrieve the RTU info from the EP0' data output register 1 */
#define MINISWCH_EP0_FIFO_R1_MAC_LO_MASK      WBGEN2_GEN_MASK(0, 32)
#define MINISWCH_EP0_FIFO_R1_MAC_LO_SHIFT     0
#define MINISWCH_EP0_FIFO_R1_MAC_LO_W(value)  WBGEN2_GEN_WRITE(value, 0, 32)
#define MINISWCH_EP0_FIFO_R1_MAC_LO_R(reg)    WBGEN2_GEN_READ(reg, 0, 32)

/* definitions for register: FIFO 'FIFO to retrieve the RTU info from the EP0' data output register 2 */

/* definitions for field: AGE in reg: FIFO 'FIFO to retrieve the RTU info from the EP0' data output register 2 */
#define MINISWCH_EP0_FIFO_R2_MAC_AGE_MASK     WBGEN2_GEN_MASK(0, 6)
#define MINISWCH_EP0_FIFO_R2_MAC_AGE_SHIFT    0
#define MINISWCH_EP0_FIFO_R2_MAC_AGE_W(value) WBGEN2_GEN_WRITE(value, 0, 6)
#define MINISWCH_EP0_FIFO_R2_MAC_AGE_R(reg)   WBGEN2_GEN_READ(reg, 0, 6)

/* definitions for register: FIFO 'FIFO to retrieve the RTU info from the EP0' control/status register */

/* definitions for field: FIFO empty flag in reg: FIFO 'FIFO to retrieve the RTU info from the EP0' control/status register */
#define MINISWCH_EP0_FIFO_CSR_EMPTY           WBGEN2_GEN_MASK(17, 1)

/* definitions for field: FIFO counter in reg: FIFO 'FIFO to retrieve the RTU info from the EP0' control/status register */
#define MINISWCH_EP0_FIFO_CSR_USEDW_MASK      WBGEN2_GEN_MASK(0, 3)
#define MINISWCH_EP0_FIFO_CSR_USEDW_SHIFT     0
#define MINISWCH_EP0_FIFO_CSR_USEDW_W(value)  WBGEN2_GEN_WRITE(value, 0, 3)
#define MINISWCH_EP0_FIFO_CSR_USEDW_R(reg)    WBGEN2_GEN_READ(reg, 0, 3)

/* definitions for register: FIFO 'FIFO to retrieve the RTU info from the EP1' data output register 0 */

/* definitions for field: MAC address most-significant part in reg: FIFO 'FIFO to retrieve the RTU info from the EP1' data output register 0 */
#define MINISWCH_EP1_FIFO_R0_MAC_HI_MASK      WBGEN2_GEN_MASK(0, 16)
#define MINISWCH_EP1_FIFO_R0_MAC_HI_SHIFT     0
#define MINISWCH_EP1_FIFO_R0_MAC_HI_W(value)  WBGEN2_GEN_WRITE(value, 0, 16)
#define MINISWCH_EP1_FIFO_R0_MAC_HI_R(reg)    WBGEN2_GEN_READ(reg, 0, 16)

/* definitions for register: FIFO 'FIFO to retrieve the RTU info from the EP1' data output register 1 */

/* definitions for field: MAC address least-significant part in reg: FIFO 'FIFO to retrieve the RTU info from the EP1' data output register 1 */
#define MINISWCH_EP1_FIFO_R1_MAC_LO_MASK      WBGEN2_GEN_MASK(0, 32)
#define MINISWCH_EP1_FIFO_R1_MAC_LO_SHIFT     0
#define MINISWCH_EP1_FIFO_R1_MAC_LO_W(value)  WBGEN2_GEN_WRITE(value, 0, 32)
#define MINISWCH_EP1_FIFO_R1_MAC_LO_R(reg)    WBGEN2_GEN_READ(reg, 0, 32)

/* definitions for register: FIFO 'FIFO to retrieve the RTU info from the EP1' data output register 2 */

/* definitions for field: AGE in reg: FIFO 'FIFO to retrieve the RTU info from the EP1' data output register 2 */
#define MINISWCH_EP1_FIFO_R2_MAC_AGE_MASK     WBGEN2_GEN_MASK(0, 6)
#define MINISWCH_EP1_FIFO_R2_MAC_AGE_SHIFT    0
#define MINISWCH_EP1_FIFO_R2_MAC_AGE_W(value) WBGEN2_GEN_WRITE(value, 0, 6)
#define MINISWCH_EP1_FIFO_R2_MAC_AGE_R(reg)   WBGEN2_GEN_READ(reg, 0, 6)

/* definitions for register: FIFO 'FIFO to retrieve the RTU info from the EP1' control/status register */

/* definitions for field: FIFO empty flag in reg: FIFO 'FIFO to retrieve the RTU info from the EP1' control/status register */
#define MINISWCH_EP1_FIFO_CSR_EMPTY           WBGEN2_GEN_MASK(17, 1)

/* definitions for field: FIFO counter in reg: FIFO 'FIFO to retrieve the RTU info from the EP1' control/status register */
#define MINISWCH_EP1_FIFO_CSR_USEDW_MASK      WBGEN2_GEN_MASK(0, 3)
#define MINISWCH_EP1_FIFO_CSR_USEDW_SHIFT     0
#define MINISWCH_EP1_FIFO_CSR_USEDW_W(value)  WBGEN2_GEN_WRITE(value, 0, 3)
#define MINISWCH_EP1_FIFO_CSR_USEDW_R(reg)    WBGEN2_GEN_READ(reg, 0, 3)

PACKED struct MINISWCH_WB {
  /* [0x0]: REG Neighbour Host: MAC address high part register */
  uint32_t NB_MACH;
  /* [0x4]: REG Neighbour Host: MAC address low part register */
  uint32_t NB_MACL;
  /* [0x8]: REG Neighbour host control register */
  uint32_t NB_CRTL;
  /* [0xc]: REG Register to manage the RTU fifo */
  uint32_t MNG_RTU;
  /* [0x10]: REG FIFO 'FIFO to retrieve the RTU info from the EP0' data output register 0 */
  uint32_t EP0_FIFO_R0;
  /* [0x14]: REG FIFO 'FIFO to retrieve the RTU info from the EP0' data output register 1 */
  uint32_t EP0_FIFO_R1;
  /* [0x18]: REG FIFO 'FIFO to retrieve the RTU info from the EP0' data output register 2 */
  uint32_t EP0_FIFO_R2;
  /* [0x1c]: REG FIFO 'FIFO to retrieve the RTU info from the EP0' control/status register */
  uint32_t EP0_FIFO_CSR;
  /* [0x20]: REG FIFO 'FIFO to retrieve the RTU info from the EP1' data output register 0 */
  uint32_t EP1_FIFO_R0;
  /* [0x24]: REG FIFO 'FIFO to retrieve the RTU info from the EP1' data output register 1 */
  uint32_t EP1_FIFO_R1;
  /* [0x28]: REG FIFO 'FIFO to retrieve the RTU info from the EP1' data output register 2 */
  uint32_t EP1_FIFO_R2;
  /* [0x2c]: REG FIFO 'FIFO to retrieve the RTU info from the EP1' control/status register */
  uint32_t EP1_FIFO_CSR;
};


#define MINISWCH_PERIPH_PREFIX                "MINISWCH"
#define MINISWCH_PERIPH_NAME                  "Wishbone Mini Switch"
#define MINISWCH_PERIPH_DESC                  WBGEN2_DESC("Wishbone Registers to configure the Mini Switch")

#endif
