/*
 * This work is part of the WR-LEN project
 *
 * Author: Emilio Marín-López <emilio<at>sevensols.com>
 *
 */
#include "types.h"
#include "eth_subsystem.h"

#ifndef MINISWCH_H_
#define MINISWCH_H_

void ms_init();
void ms_reset_rtu_ep(int port_num);
void ms_eth_update();
void ms_eth_print_rtu();

#endif /* MINISWCH_H_ */
