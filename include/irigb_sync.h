/*
 * irigb_sync.h
 *
 *  Created on: Oct 24, 2014
 *      Author: Huse Gomiz
 */

#ifndef IRIGB_SYNC_H_
#define IRIGB_SYNC_H_

#include "types.h"

void irigbsync();

#endif /* IRIGB_SYNC_H_ */


