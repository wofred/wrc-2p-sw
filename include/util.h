#ifndef __UTIL_H
#define __UTIL_H
#include <inttypes.h>

/* Color codes for cprintf()/pcprintf() */
#define C_DIM 0x80
#define C_WHITE 7
#define C_GREY (C_WHITE | C_DIM)
#define C_RED 1
#define C_GREEN 2
#define C_BLUE 4

/* Return TAI date/time in human-readable form. Non-reentrant. */
char *format_time(uint64_t sec);

/* Color printf() variant. */
void cprintf(int color, const char *fmt, ...);

/* Color printf() variant, sets curspor position to (row, col) before printing. */
void pcprintf(int row, int col, int color, const char *fmt, ...);

/* Clears the terminal scree. */
void term_clear();

 /* Function to read the values longer than 32 bits from the command line. */
uint64_t parse_64bits_args(char *argument);

/* Function to print the values longer than 32 bits */
void print_longer_than_32_bits(uint64_t value);

#endif
