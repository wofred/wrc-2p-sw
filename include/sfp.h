/* SFP Detection / management functions */

#ifndef __SFP_H
#define __SFP_H

#include <stdint.h>

/*
 * Default values
 */

#define SFP_ALPHA_DEFAULT		73622176
#define SFP_DELTATX_DEFAULT		46407
#define SFP_DELTARX_DEFAULT		167843

/* Returns 1 if there's a SFP transceiver inserted in the socket. */
int sfp_present(int port);

/* Reads the part ID of the SFP from its configuration EEPROM */
int sfp_read_part_id(char *part_id, int port);

/* Detects and matches the SFP plugged into the port */
int sfp_detect_and_match(uint8_t port);

#endif
