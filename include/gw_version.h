/*
 * This work is part of the White Rabbit project
 *
 * Copyright (C) 2016
 *
 * Released according to the GNU GPL, version 2 or any later version.
 */

#define GW_REG_BOARD_ID		0x0
#define GW_REG_MINOR_VER	0x4
#define GW_REG_DATE_VER		0x8

void print_gw_info(void);
