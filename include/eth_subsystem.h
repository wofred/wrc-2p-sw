/*
 * irigb_sync.h
 *
 *  Created on: 2015-06-01
 *      Author: Emilio Marín emilio<at>sevensols.com
 */

#include "types.h"

void eth_subsystem_init();
int eth_link_detection();
void eth_restart_autoneg();

