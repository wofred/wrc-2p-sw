#include "i2c.h"

#define PCA9554_INPUT_REG 0x0
#define PCA9554_OUTPUT_REG 0x1
#define PCA9554_POLARITY_REG 0x2
#define PCA9554_CONFIG_REG 0x3

#define PCA9554_ZEN_INPUT_I2C 	0x20
#define PCA9554_ZEN_OUTPUT_I2C	0x21
#define PCA9554_LEN_FT_I2C	0x20

#define I2C_REFRESH_TIME TICS_PER_SECOND

#define I2C_ADDR_TO_WRITE(addr) (addr << 1)
#define I2C_ADDR_TO_READ(addr) (I2C_ADDR_TO_WRITE(addr) | 1)

unsigned char read_i2c_pca9554(uint8_t i2cif, unsigned char addr);
void write_i2c_pca9554(uint8_t i2cif, unsigned char addr, unsigned char data);
void configure_i2c_pca9554(uint8_t i2cif, unsigned char addr, unsigned char data);
