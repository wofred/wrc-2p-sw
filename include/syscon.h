#ifndef __SYSCON_H
#define __SYSCON_H

#include <inttypes.h>
#include <sys/types.h>


#include "board.h"
#undef PACKED /* if we already included a regs file, we'd get a warning */
#include <hw/wrc_syscon_regs.h>

#ifdef CONFIG_LENTRIG
	#include "len_fine_tune.h"
#endif

struct SYSCON_WB {
	uint32_t RSTR;		/*Syscon Reset Register */
	uint32_t GPSR;		/*GPIO Set/Readback Register */
	uint32_t GPCR;		/*GPIO Clear Register */
	uint32_t HWFR;		/*Hardware Feature Register */
	uint32_t TCR;		/*Timer Control Register */
	uint32_t TVR;		/*Timer Counter Value Register */
};

/*GPIO pins*/
#define GPIO_LED_LINK SYSC_GPSR_LED_LINK
#define GPIO_LED_STAT SYSC_GPSR_LED_STAT
#define GPIO_BTN1     SYSC_GPSR_BTN1
#define GPIO_BTN2     SYSC_GPSR_BTN2
#define GPIO_SFP0_DET  SYSC_GPSR_SFP0_DET
#define GPIO_SFP1_DET  SYSC_GPSR_SFP1_DET

#define WRPC_FMC_I2C  0
#define WRPC_SFP0_I2C  1
#define WRPC_SFP1_I2C  2

struct s_i2c_if {
	uint32_t scl;
	uint32_t sda;
};

#ifdef CONFIG_LENTRIG
	extern struct s_i2c_if i2c_if[4];
#else
	extern struct s_i2c_if i2c_if[3];
#endif

#define TICS_PER_SECOND 1000
void timer_init(uint32_t enable);
uint32_t timer_get_tics();
void timer_delay(uint32_t how_long);

/* usleep.c */
extern void usleep_init(void);
extern int usleep(useconds_t usec);

extern volatile struct SYSCON_WB *syscon;

#ifdef CONFIG_LENTRIG
	/* The LEN Connector I2C bus is connected into the its GPIOs */
	extern volatile uint32_t *GPIO_ADDR;
#endif

/****************************
 *        GPIO
 ***************************/
static inline void gpio_out(int pin, int val){


#ifdef CONFIG_LENTRIG
  /* There is a I2C line connected into the GPIOs on the WR-LEN.
   * This code enables its use.
   */
	if(pin == WR_LEN_CONN_SCL || pin == WR_LEN_CONN_SDA){
		if(val)
			wr_len_gpio_set(pin);
		else
			wr_len_gpio_clr(pin);

	} else {
		if (val)
			syscon->GPSR = pin;
		else
			syscon->GPCR = pin;
	}
#else
	if (val)
		syscon->GPSR = pin;
	else
		syscon->GPCR = pin;
#endif
}

static inline int gpio_in(int pin)
{
#ifdef CONFIG_LENTRIG
  /*
   * The LEN connector I2C is never read by the LM32 thus, there is no code to write...
   */
#endif
	return syscon->GPSR & pin ? 1 : 0;

}

static inline int sysc_get_memsize()
{
	return (SYSC_HWFR_MEMSIZE_R(syscon->HWFR) + 1) * 16;
}

#endif
