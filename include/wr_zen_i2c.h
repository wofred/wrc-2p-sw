#ifndef WR_ZEN_I2C_H
#define WR_ZEN_I2C_H

#include <wrc.h>
#include <pca9554.h>

#define I2C_SWITCH_ADDR 0x70
#define I2C_SWITCH_DFL_CFG 0x2c

int read_i2c_switch(uint8_t i2cif,uint8_t sw_addr);
int write_i2c_switch(uint8_t i2cif,uint8_t sw_addr, uint8_t value);
int configure_i2c_switch(uint8_t i2cif,uint8_t sw_addr, uint8_t sw_config);

#define WR_LINK1_LOS_TX_FAULT 0x0
#define WR_LINK1_DETECT 0x1
#define WR_LINK2_LOS_TX_FAULT 0x2
#define WR_LINK2_DETECT 0x3


/** SCH configuration V2.0
 * **************************
#define WR_LINK2_LED_LINK 0x0
#define WR_LINK2_LED_WRMODE 0x1
#define SFP2_TX_DISABLE 0x2
#define WR_LINK2_LED_SYNCED 0x3 
#define WR_LINK1_LED_WRMODE 0x4
#define WR_LINK1_LED_SYNCED 0x5
#define WR_LINK1_LED_LINK 0x6 
#define SFP1_TX_DISABLE 0x7
**/

// Custom configuration
#define WR_LINK2_LED_WRMODE 0x0
#define WR_LINK2_LED_LINK 0x1
#define SFP2_TX_DISABLE 0x2
#define WR_LINK2_LED_SYNCED 0x3 
#define WR_LINK1_LED_LINK 0x4 
#define WR_LINK1_LED_SYNCED 0x5
#define WR_LINK1_LED_WRMODE 0x6
#define SFP1_TX_DISABLE 0x7

#define GEN_OR_MASK(x) (1 << x)
#define GEN_AND_MASK(x) (~GEN_OR_MASK(x))
#define GET_GPIO(val,n_gpio) (((val & GEN_OR_MASK(n_gpio)) != 0) ? 1 : 0)

#define WB_GPIO_SFP2_TX_LOS_FAULT 0x0
#define WB_GPIO_SFP2_DET 0x1
#define WB_GPIO_SFP1_TX_LOS_FAULT 0x2
#define WB_GPIO_SFP1_DET 0x3
#define WB_GPIO_GTP_RESET 0x4
#define WB_GPIO_SW_RESET 0x5

#define WB_GPIO_SFP1_TX_DIS 0x0
#define WB_GPIO_SFP2_TX_DIS 0x1

#define N_FPGA_GPIO_BANK 32

#define N_GPIOS 6

#define FPGA_GPIO_CLR_ADDR(N) ((N/N_FPGA_GPIO_BANK)*0x20)
#define FPGA_GPIO_SET_ADDR(N) ((N/N_FPGA_GPIO_BANK)*0x20 + 0x4)
#define FPGA_GPIO_DIR_ADDR(N) ((N/N_FPGA_GPIO_BANK)*0x20 + 0x8)
#define FPGA_GPIO_STA_ADDR(N) ((N/N_FPGA_GPIO_BANK)*0x20 + 0xc)

#define FPGA_GPIO_SET_DATA(N) (0x1 << (N%N_FPGA_GPIO_BANK))
#define FPGA_GPIO_CLR_DATA(N) (FPGA_GPIO_SET_DATA(N))

void wr_zen_i2c_sfp_init();
void wr_zen_link_led(int state, int nsfp);
void wr_zen_sfp_i2c_update(uint8_t initial_check);
void wr_zen_gpio_gtp_rst();
void wr_zen_gpio_init();

#endif
