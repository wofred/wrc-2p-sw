/* This work is part of the WR-LEN project
 *
 * emilio<AT>sevensols.com
 *
 */

#define CONF1_XADC_ADDR 0x300
#define CONF2_XADC_ADDR 0x304
#define CONF3_XADC_ADDR 0x308

#define TEMP_XADC_ADDR 0x200


void xadc_temp_init();
void xadc_temp_read(uint16_t  * temp_fpga, uint16_t  * temp_fpga_dec);
