/*
 * This work is part of the White Rabbit project
 *
 * Copyright (C) 2011,2012 CERN (www.cern.ch)
 * Author: Aurelio Colosimo <aurelio@aureliocolosimo.it>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 */

#include <inttypes.h>
#include <wrc.h>
#include <w1.h>
#include <ppsi/ppsi.h>
#include <wrpc.h>
#include <wr-api.h>
#include <minic.h>
#include <softpll_ng.h>
#include <syscon.h>
#include <pps_gen.h>
#include <onewire.h>
#include <util.h>
#include "wrc_ptp.h"
#include "hal_exports.h"
#include "lib/ipv4.h"

/* import wrlen_num_ports constant */
#include "wrc.h"

#define UI_REFRESH_PERIOD TICS_PER_SECOND	/* 1 sec */

struct ptpdexp_sync_state_t;
extern ptpdexp_sync_state_t cur_servo_state;		//One per device.
extern int wrc_man_phase;							//One per device.

extern struct pp_servo servo;						//One per device.

extern struct pp_instance ppi_static_0;
extern struct pp_instance ppi_static_1;

struct pp_instance *ppi[wrlen_num_ports] = {&ppi_static_0, &ppi_static_1};
static hexp_port_state_t global_ps[wrlen_num_ports];

extern int ptp_mode;

static void wrc_mon_std_servo(int port);

#define PRINT64_FACTOR	1000000000LL
char* print64(uint64_t x)
{
	uint32_t h_half, l_half;
	static char buf[2*10+1];	//2x 32-bit value + \0
	if (x < PRINT64_FACTOR)
		sprintf(buf, "%u", (uint32_t)x);
	else{
		l_half = __div64_32(&x, PRINT64_FACTOR);
		h_half = (uint32_t) x;
		sprintf(buf, "%u%u", h_half, l_half);
	}
	return buf;

}

int wrc_check_slave_port(){

	if (ptp_mode == WRC_SLAVE_WR0)
		return 0;
	else if (ptp_mode == WRC_SLAVE_WR1)
		return 1;
	else
		return -1;
}

int wrc_mon_status(int port)
{
	struct pp_state_table_item *ip = NULL;
	for (ip = pp_state_table; ip->state != PPS_END_OF_TABLE; ip++) {
		if (ip->state == ppi[port]->state)
			break;
	}

	cprintf(C_BLUE, "\n\nPTP status: ");
	cprintf(C_WHITE, "%s\n",ppi[port]->iface_name);

	if ((ptp_mode != WRC_SLAVE_WR1) || (ptp_mode != WRC_SLAVE_WR0)) {
		cprintf(C_RED, "\nMaster Mode\n\n");
	}else{
		/* show_servo */
		cprintf(C_BLUE, "\n\nSynchronization status:\n\n");
	}

	return 1;
}

void wrc_init_stats(void)
{
	//Init the statistic structure
	wrc_stats_head->magic[0]='W';
	wrc_stats_head->magic[1]='R';
	wrc_stats_head->magic[2]='2';
	wrc_stats_head->magic[3]='P';
	wrc_stats_head->magic[4]='C';
	wrc_stats_head->magic[5]='-';
	wrc_stats_head->magic[6]='-';
	wrc_stats_head->magic[7]='\0';


	wrc_stats_head->ps0_off=(void*)&global_ps[0];
	wrc_stats_head->ps1_off=(void*)&global_ps[1];
	wrc_stats_head->servo_off=(void*)&cur_servo_state;
	//void spll_init() initialize wrc_stats_head->spll_off

}


void wrc_dump_refresh(uint8_t force)
{
	static uint32_t last = 0;
	int i;
	hexp_port_state_t *ps=&(global_ps);
	char ifname[4]="wr0";

	// Out cause there is not enough tics.
	if ((timer_get_tics() - last < ext_refresh_time) & (force == 0))
		return;

	last = timer_get_tics();

	shw_pps_gen_get_time(&(cur_servo_state.sec), &(cur_servo_state.nsec));
	cur_servo_state.mode=ptp_mode;

	int32_t temp;
	//first read the value from previous measurement,
	//first one will be random, I know
	temp = w1_read_temp_bus(&wrpc_w1_bus, W1_FLAG_COLLECT);
	//then initiate new conversion for next loop cycle
	w1_read_temp_bus(&wrpc_w1_bus, W1_FLAG_NOWAIT);
	cur_servo_state.temp=temp >> 16;
	cur_servo_state.temp_dec=((temp & 0xffff) * 1000 >> 16);



	for (i=0; i < wrlen_num_ports; i++){
		ifname[2]='0'+i;
		halexp_get_port_state(&ps[i], ifname);
		minic_get_stats(&(ps[i].tx_count), &(ps[i].rx_count), i);
	}
	//pp_printf("refreshed:%d %d: @ %p\n",ps[0].tx_count, ps[0].rx_count, &(ps[0].tx_count));
}

void wrc_mon_gui(void)
{
	static uint32_t last = 0;
	hexp_port_state_t *ps=&(global_ps);
	int tx, rx;
	int aux_stat;
	uint64_t sec;
	uint32_t nsec;

	int8_t print_WR_params = FALSE;

	char *wrmode_table[5]= {"WRC_MODE_UNKNOWN", "WRC_MODE_GM", "WRC_MODE_MASTER", "WRC_SLAVE_WR1", "WRC_SLAVE_WR0"};

	int i;
#ifdef CONFIG_ETHERBONE
	uint8_t ip[4];
#endif

	if (timer_get_tics() - last < UI_REFRESH_PERIOD)
		return;

	last = timer_get_tics();

	term_clear();

	pcprintf(1, 1, C_BLUE, "WR PTP Core Sync Monitor: PPSI - %s", board_name_ext);
	pcprintf(2, 1, C_GREY, "Esc = exit");


	cprintf(C_BLUE, "\n\nTAI Time:                  ");
	cprintf(C_WHITE, "%s", format_time(cur_servo_state.sec));

	/*show wr ptp mode */
	pcprintf(4, 1, C_GREEN, "\n\nWR-LEN mode : %s ", wrmode_table[ptp_mode]);

	pp_printf("\n-----------------------\n");

	/*show_ports */
	pcprintf(6, 1, C_BLUE, "\n\nLink status:");

	for (i=0; i < wrlen_num_ports; i++)
	{

		cprintf(C_WHITE, "\n\n wr%i : ", i);
		if (ps[i].up)
			cprintf(C_GREEN, "Link up   ");
		else
			cprintf(C_RED, "Link down ");

		if (ps[i].up) {
			cprintf(C_GREY, "(RX: %d, TX: %d), mode: ", ps[i].rx_count, ps[i].tx_count);

			switch (ptp_mode) {
			case WRC_MODE_GM:
			case WRC_MODE_MASTER:
				cprintf(C_WHITE, "WR Master  ");
				break;
			case WRC_SLAVE_WR0:
				if (i == 0)
					cprintf(C_WHITE, "WR Slave   ");
				else
					cprintf(C_WHITE, "WR Master   ");
				break;
			case WRC_SLAVE_WR1:
				if (i == 1)
					cprintf(C_WHITE, "WR Slave   ");
				else
					cprintf(C_WHITE, "WR Master   ");
				break;
			default:
				cprintf(C_RED, "WR Unknown   ");
				break;
			}

			if (ps[i].is_locked)
				cprintf(C_GREEN, "Locked  ");
			else
				cprintf(C_RED, "NoLock  ");
			if (ps[i].rx_calibrated && ps[i].tx_calibrated)
				cprintf(C_GREEN, "Calibrated  ");
			else
				cprintf(C_RED, "Uncalibrated  ");
	#ifdef CONFIG_ETHERBONE
			cprintf(C_WHITE, "\n IPv4: ");
			getIP(ip);
			if (needIP)
				cprintf(C_RED, "BOOTP running");
			else
				cprintf(C_GREEN, "%d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3]);

	#endif

			}

			/* Condition for the WR monitor */
			if (WR_DSPOR(ppi[i])->wrModeOn && (wrc_check_slave_port() == i)) {
				print_WR_params = TRUE;
			}

		}

		pp_printf("\n-----------------------\n");

		/* Standard PTP monitor */
		if (!print_WR_params & cur_servo_state.valid){
			if (ptp_mode == WRC_SLAVE_WR0 || ptp_mode == WRC_SLAVE_WR1) {
				wrc_mon_std_servo(wrc_check_slave_port());
			}
			return;
		}

		if (!cur_servo_state.valid) {
			cprintf(C_RED, "\nMaster mode or sync info not valid\n\n");
			return;
		}

		cprintf(C_GREY, "\nServo state:               ");
		cprintf(C_WHITE, "%s\n", cur_servo_state.slave_servo_state);
		cprintf(C_GREY, "Phase tracking:            ");

		if (cur_servo_state.tracking_enabled)
			cprintf(C_GREEN, "ON\n");
		else
			cprintf(C_RED, "OFF\n");
		cprintf(C_GREY, "Synchronization source:    ");
		cprintf(C_WHITE, "%s\n", cur_servo_state.sync_source);

		cprintf(C_BLUE, "\nTiming parameters:\n\n");

		cprintf(C_GREY, "Round-trip time (mu):    ");
		cprintf(C_WHITE, "%s ps\n", print64(cur_servo_state.mu));
		cprintf(C_GREY, "Master-slave delay:      ");
		cprintf(C_WHITE, "%s ps\n", print64(cur_servo_state.delay_ms));
		cprintf(C_GREY, "Master PHY delays:       ");
		cprintf(C_WHITE, "TX: %d ps, RX: %d ps\n",
			(int32_t) cur_servo_state.delta_tx_m,
			(int32_t) cur_servo_state.delta_rx_m);
		cprintf(C_GREY, "Slave PHY delays:        ");
		cprintf(C_WHITE, "TX: %d ps, RX: %d ps\n",
			(int32_t) cur_servo_state.delta_tx_s,
			(int32_t) cur_servo_state.delta_rx_s);
		cprintf(C_GREY, "Total link asymmetry:    ");
		cprintf(C_WHITE, "%9d ps\n",
			(int32_t) (cur_servo_state.total_asymmetry));
		cprintf(C_GREY, "Cable rtt delay:         ");
		cprintf(C_WHITE, "%s ps\n", print64(cur_servo_state.mu -
					cur_servo_state.delta_tx_m -
					cur_servo_state.delta_rx_m -
					cur_servo_state.delta_tx_s -
					cur_servo_state.delta_rx_s));
		cprintf(C_GREY, "Clock offset:            ");
		cprintf(C_WHITE, "%9d ps\n",
			(int32_t) (cur_servo_state.cur_offset));
		cprintf(C_GREY, "Phase setpoint:          ");
		cprintf(C_WHITE, "%9d ps\n",
			(int32_t) (cur_servo_state.cur_setpoint));
		cprintf(C_GREY, "Skew:                    ");
		cprintf(C_WHITE, "%9d ps\n",
			(int32_t) (cur_servo_state.cur_skew));
		cprintf(C_GREY, "Manual phase adjustment: ");
		cprintf(C_WHITE, "%9d ps\n", (int32_t) (wrc_man_phase));

		cprintf(C_GREY, "Update counter:          ");
		cprintf(C_WHITE, "%9d \n",
			(int32_t) (cur_servo_state.update_count));


		pp_printf("--");

		return;
}

static inline void cprintf_ti(int color, struct TimeInternal *ti)
{
	if ((ti->seconds > 0) ||
		((ti->seconds == 0) && (ti->nanoseconds >= 0)))
		cprintf(color, "%2i.%09i s", ti->seconds, ti->nanoseconds);
	else {
		if (ti->seconds == 0)
			cprintf(color, "-%i.%09i s", ti->seconds, -ti->nanoseconds);
		else
			cprintf(color, "%2i.%09i s", ti->seconds, -ti->nanoseconds);
	}
}

static void wrc_mon_std_servo(int port)
{
	cprintf(C_RED, "\nWR Off");

	if (wrc_mon_status(port) == 0)
		return;

	cprintf(C_GREY, "Clock offset:                 ");

	if (DSCUR(ppi[port])->offsetFromMaster.seconds)
		cprintf_ti(C_WHITE, &DSCUR(ppi[port])->offsetFromMaster);
	else {
		cprintf(C_WHITE, "%9i ns", DSCUR(ppi[port])->offsetFromMaster.nanoseconds);

		cprintf(C_GREY, "\nOne-way delay averaged:       ");
		cprintf(C_WHITE, "%9i ns", DSCUR(ppi[port])->meanPathDelay.nanoseconds);

		cprintf(C_GREY, "\nObserved drift:               ");
		cprintf(C_WHITE, "%9i ns \n", SRV(ppi[port])->obs_drift);
	}
}


int wrc_log_stats(uint8_t onetime)
{
	static uint32_t last = 0;
	hexp_port_state_t *ps=&(global_ps);
	int tx, rx;
	int aux_stat;

	uint16_t temp_fpga;
	uint16_t temp_fpga_dec;

	wrc_dump_refresh(1);

	int i;
	char *wrmode_table[5]= {"WRC_MODE_UNKNOWN", "WRC_MODE_GM", "WRC_MODE_MASTER", "WRC_SLAVE_WR1", "WRC_SLAVE_WR0"};

	// Out cause there is not enough tics.
	if (!onetime && timer_get_tics() - last < ext_refresh_time)
		return 0;

	last = timer_get_tics();



	pp_printf("WR mode : %s \n", wrmode_table[ptp_mode]);

	for (i=0; i < wrlen_num_ports; i++){
		pp_printf("wr%i -> ", i);
		pp_printf("lnk:%d rx:%d tx:%d ", ps[i].up, ps[i].rx_count, ps[i].tx_count);
		pp_printf("lock:%d ", ps[i].is_locked ? 1 : 0);

		if ((atoi(&cur_servo_state.sync_source[2]) == i) && cur_servo_state.valid ){
			pp_printf("syncs:%s ", cur_servo_state.sync_source);
			pp_printf("sv:%d ", cur_servo_state.valid ? 1 : 0);
			pp_printf("ss:'%s' ", cur_servo_state.slave_servo_state);
			aux_stat = spll_get_aux_status(0);
			pp_printf("aux:%x ", aux_stat);
			pp_printf("sec:%d nsec:%d ", (uint32_t) cur_servo_state.sec, cur_servo_state.nsec);	/* fixme: clock is not always 125 MHz */
			pp_printf("mu:%s ", print64(cur_servo_state.mu));
			pp_printf("dms:%s ", print64(cur_servo_state.delay_ms));
			pp_printf("dtxm:%d drxm:%d ", (int32_t) cur_servo_state.delta_tx_m, (int32_t) cur_servo_state.delta_rx_m);
			pp_printf("dtxs:%d drxs:%d ", (int32_t) cur_servo_state.delta_tx_s, (int32_t) cur_servo_state.delta_rx_s);
			pp_printf("asym:%d ", (int32_t) (cur_servo_state.total_asymmetry));
			pp_printf("crtt:%s ", print64(cur_servo_state.mu - cur_servo_state.delta_tx_m -
					cur_servo_state.delta_rx_m - cur_servo_state.delta_tx_s - cur_servo_state.delta_rx_s));
			pp_printf("cko:%d ", (int32_t) (cur_servo_state.cur_offset));
			pp_printf("setp:%d ", (int32_t) (cur_servo_state.cur_setpoint));
			pp_printf("hd:%d md:%d ad:%d ", spll_get_dac(-1), spll_get_dac(0), spll_get_dac(1));
			pp_printf("ucnt:%d ", (int32_t) cur_servo_state.update_count);
		}
		pp_printf("\n");
	}

	pp_printf("temp: %d.%03d C ", cur_servo_state.temp, cur_servo_state.temp_dec);

#ifdef CONFIG_XADC_TEMP
	xadc_temp_read(&temp_fpga, &temp_fpga_dec);
	pp_printf("temp-FPGA: %d.%d C", temp_fpga, temp_fpga_dec);
#endif

	pp_printf("\n");
	return 0;
}
