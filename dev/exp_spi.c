/*
 * This work is part of the White Rabbit project
 *
 * Copyright (C) 2016 (Based on previous works from the wr-switch-sw)
 * Author: Tomasz Wlostowski <tomasz.wlostowski@cern.ch>
 * Author: Alessandro Rubini <rubini@gnudd.com>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 */

/*
 * SPI sw controller to handle the WR-LEN Expansion SPI peripheral.
 *
 * SPI stuff, used by later code
 */
#include <wrc.h>
#include "exp_spi.h"

/* Command to write into the SPI controller registers through WB */
static inline void wb_exp_spi_writel(uint32_t data, uint32_t reg)
{
	*(volatile uint32_t *)(BASE_WB_EXPANSION_SPI + reg) = data;		//Instruction to write at physical memory
}

/* Command to read from the SPI controller registers through WB */
static inline uint32_t wb_exp_spi_readl(uint32_t reg)
{
	return *(volatile uint32_t *)(BASE_WB_EXPANSION_SPI + reg);		//Instruction to read from the physical memory
}

/* To initialize the SPI bus clock */
void exp_spi_init(){
	mprintf("SPI bus init. \n");
	wb_exp_spi_writel(100, SPI_REG_DIVIDER);
}

/* Middle function to make the adaptation from the WB from to the SPI bus. */
static int exp_spi_txrx(int ss, int nbits, uint32_t in, uint32_t *out)
{
	uint32_t rval;

	if (!out)
		out = &rval;

	wb_exp_spi_writel(SPI_CTRL_ASS | SPI_CTRL_CHAR_LEN(nbits) | SPI_CTRL_TXNEG, SPI_REG_CTRL);

	wb_exp_spi_writel(in, SPI_REG_TX0);
	wb_exp_spi_writel((1 << ss), SPI_REG_SS);
	wb_exp_spi_writel(SPI_CTRL_ASS | SPI_CTRL_CHAR_LEN(nbits) | SPI_CTRL_TXNEG | SPI_CTRL_GO_BSY, SPI_REG_CTRL);

	while(wb_exp_spi_readl(SPI_REG_CTRL) & SPI_CTRL_GO_BSY);
		*out = wb_exp_spi_readl(SPI_REG_RX0);
	return 0;
}

/* To write into the SPI slave through the Expansion Connector */
void exp_spi_write_reg(uint8_t reg, uint8_t val)
{
	exp_spi_txrx(CS_PLL, 16, (reg << (8 + 3)) | val, NULL); // + 3 because the PCA9502 addressing.
}

/* To read from the SPI slave through the Expansion Connector */
uint8_t exp_spi_read_reg(uint8_t reg)
{
	uint32_t rval;
	exp_spi_txrx(CS_PLL, 16, (reg << (8 + 3)) | (1 << 15), &rval); // + 3 because the PCA9502 addressing.
	return rval & 0xff;
}

