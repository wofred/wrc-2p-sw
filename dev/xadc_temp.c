/* This work is part of the WR-LEN project
 *
 * emilio<AT>sevensols.com
 *
 */

#include <wrc.h>
#include "xadc_temp.h"

volatile void *XADC_TEMP;

/* Command to write into the XADC_TEMP controller registers through WB */
static inline void wb_xadc_writel(uint32_t data, uint32_t reg)
{
	*(volatile uint32_t *)(XADC_TEMP + reg) = data;		//Instruction to write at physical memory
}

/* Command to read from the XADC_TEMP controller registers through WB */
static inline uint32_t wb_xadc_readl(uint32_t reg)
{
	return *(volatile uint32_t *)(XADC_TEMP + reg);		//Instruction to read from the physical memory
}

/*
 * It initializes the XADC_TEMP module
 */
void xadc_temp_init(){

  XADC_TEMP = (volatile void *)BASE_XADC_TEMP;

}

/*
 * It reads the temperature from the internal FPGA XADC.
 */

void xadc_temp_read(uint16_t  * temp_fpga, uint16_t * temp_fpga_dec){

  uint16_t temp_readout, temp_conv, temp_conv_dec;

  temp_readout = wb_xadc_readl(TEMP_XADC_ADDR);

  temp_conv = (((temp_readout * 504) * 100) >> 16) - 27300;

  temp_conv_dec = temp_conv/100;

  temp_conv_dec = temp_conv - (temp_conv_dec*100);

  temp_conv = (temp_conv - temp_conv_dec)/100;

  *temp_fpga_dec = (uint16_t) temp_conv_dec;

  *temp_fpga = (uint16_t) temp_conv;

}

