/*
 * Copyright (C) 2012 CERN (www.cern.ch)
 * Author: Alessandro Rubini <rubini@gnudd.com>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 *
 * This work is part of the White Rabbit project, a research effort led
 * by CERN, the European Institute for Nuclear Research.
 */
 #include <stdio.h>
 #include <stdlib.h>
 #include "shell.h"
 #include <wrpc.h>
 #include <string.h>
 #include <errno.h>
 #include "types.h"
#include "wr-dio.h"
#include "hw/wrsw_dio_wb.h"


/* We need a clear mapping for the registers of the various bits */
struct regmap {
	int trig_l;
	int trig_h;
	int cycle;
	int pulse;
	int per_sec;
	int per_cyc;
};

#define R(x) (offsetof(struct DIO_WB, x))
static struct regmap regmap[] = {
	{
		.trig_l = R(TRIG0),
		.trig_h = R(TRIGH0),
		.cycle = R(CYC0),
		.pulse = R(PROG0_PULSE),
		.per_sec = R(PER_SEC0),
		.per_cyc = R(PER_CYC0),
	}, {
		.trig_l = R(TRIG1),
		.trig_h = R(TRIGH1),
		.cycle = R(CYC1),
		.pulse = R(PROG1_PULSE),
		.per_sec = R(PER_SEC1),
		.per_cyc = R(PER_CYC1),
	}, {
		.trig_l = R(TRIG2),
		.trig_h = R(TRIGH2),
		.cycle = R(CYC2),
		.pulse = R(PROG2_PULSE),
		.per_sec = R(PER_SEC2),
		.per_cyc = R(PER_CYC2),
	}, {
		.trig_l = R(TRIG3),
		.trig_h = R(TRIGH3),
		.cycle = R(CYC3),
		.pulse = R(PROG3_PULSE),
		.per_sec = R(PER_SEC3),
		.per_cyc = R(PER_CYC3),
	}, {
		.trig_l = R(TRIG4),
		.trig_h = R(TRIGH4),
		.cycle = R(CYC4),
		.pulse = R(PROG4_PULSE),
		.per_sec = R(PER_SEC4),
		.per_cyc = R(PER_CYC4),
	}
};

volatile struct regmap *map;
volatile struct DIO_WB *dio;


/* The dio default values are restored */
void dio_reset(uint8_t ch){

  if ( (ch & 1) == 1) {
    dio->TRIG0        = DIO_TRIG0_SECONDS_W(0);
    dio->TRIGH0       = DIO_TRIGH0_SECONDS_W(0);
    dio->CYC0         = DIO_CYC0_CYC_W(0);
    dio->PROG0_PULSE  = DIO_PROG0_PULSE_LENGTH_W(0);
    dio->PER_SEC0     = DIO_PER_SEC0_SECONDS_W(0);
    dio->PER_CYC0     = DIO_PER_CYC0_PER_CYC_W(0);
    dio->IOMODE       = dio->IOMODE & DIO_IOMODE_CH0_W(0);
    dio->LATCH        = dio->LATCH & ~(DIO_LATCH_TIME_CH0);
    dio->PER          = dio->PER & 0x1e;

    dio->RESET_REG    = dio->RESET_REG | DIO_RESET_REG_RESET0;
    dio->RESET_REG    = dio->RESET_REG & ~DIO_RESET_REG_RESET0;
  }

  if ( (ch & 2) == 2) {
    dio->TRIG1        = DIO_TRIG1_SECONDS_W(0);
    dio->TRIGH1       = DIO_TRIGH1_SECONDS_W(0);
    dio->CYC1         = DIO_CYC1_CYC_W(0);
    dio->PROG1_PULSE  = DIO_PROG1_PULSE_LENGTH_W(0);
    dio->PER_SEC1     = DIO_PER_SEC0_SECONDS_W(0);
    dio->PER_CYC1     = DIO_PER_CYC0_PER_CYC_W(0);
    dio->IOMODE       = dio->IOMODE & DIO_IOMODE_CH1_W(0);
    dio->LATCH        = dio->LATCH & ~(DIO_LATCH_TIME_CH1);
    dio->PER          = dio->PER & 0x1d;

    dio->RESET_REG    = dio->RESET_REG | DIO_RESET_REG_RESET1;
    dio->RESET_REG    = dio->RESET_REG & ~DIO_RESET_REG_RESET1;
  }

  if ( (ch & 4) == 4) {
    dio->TRIG2        = DIO_TRIG2_SECONDS_W(0);
    dio->TRIGH2       = DIO_TRIGH2_SECONDS_W(0);
    dio->CYC2         = DIO_CYC2_CYC_W(0);
    dio->PROG2_PULSE  = DIO_PROG2_PULSE_LENGTH_W(0);
    dio->PER_SEC2     = DIO_PER_SEC2_SECONDS_W(0);
    dio->PER_CYC2     = DIO_PER_CYC2_PER_CYC_W(0);
    dio->IOMODE       = dio->IOMODE & 0x30;
    dio->IOMODE       = dio->IOMODE & DIO_IOMODE_CH2_W(0);
    dio->LATCH        = dio->LATCH & ~(DIO_LATCH_TIME_CH2);
    dio->PER          = dio->PER & 0x1b;

    dio->RESET_REG    = dio->RESET_REG | DIO_RESET_REG_RESET2;
    dio->RESET_REG    = dio->RESET_REG & ~DIO_RESET_REG_RESET2;
  }

  if ( (ch & 8) == 8) {
    dio->TRIG3        = DIO_TRIG3_SECONDS_W(0);
    dio->TRIGH3       = DIO_TRIGH3_SECONDS_W(0);
    dio->CYC3         = DIO_CYC3_CYC_W(0);
    dio->PROG3_PULSE  = DIO_PROG3_PULSE_LENGTH_W(0);
    dio->PER_SEC3     = DIO_PER_SEC3_SECONDS_W(0);
    dio->PER_CYC3     = DIO_PER_CYC3_PER_CYC_W(0);
    dio->IOMODE       = dio->IOMODE & DIO_IOMODE_CH3_W(0);
    dio->LATCH        = dio->LATCH & ~(DIO_LATCH_TIME_CH3);
    dio->PER          = dio->PER & 0x17;

    dio->RESET_REG    = dio->RESET_REG | DIO_RESET_REG_RESET3;
    dio->RESET_REG    = dio->RESET_REG & ~DIO_RESET_REG_RESET3;
  }

  if ( (ch & 16) == 16) {
    dio->TRIG4        = DIO_TRIG4_SECONDS_W(0);
    dio->TRIGH4       = DIO_TRIGH4_SECONDS_W(0);
    dio->CYC4         = DIO_CYC4_CYC_W(0);
    dio->PROG4_PULSE  = DIO_PROG4_PULSE_LENGTH_W(0);
    dio->PER_SEC4     = DIO_PER_SEC4_SECONDS_W(0);
    dio->PER_CYC4     = DIO_PER_CYC4_PER_CYC_W(0);
    dio->IOMODE       = dio->IOMODE & DIO_IOMODE_CH4_W(0);
    dio->LATCH        = dio->LATCH & ~(DIO_LATCH_TIME_CH4);
    dio->PER          = dio->PER & 0x0f;

    dio->RESET_REG    = dio->RESET_REG | DIO_RESET_REG_RESET4;
    dio->RESET_REG    = dio->RESET_REG & ~DIO_RESET_REG_RESET4;
  }
}


/* Initialize the GPIO base address */

void dio_init(){
  dio = BASE_WB_DIO;

  /* The values are reset */
  dio_reset(0x1f);
}

/* This programs a new pulse without changing the width */
static void wrn_new_pulse(struct wr_dio_cmd *cmd, int ch)
{
  void *base = dio;

  map = regmap + ch;

  uint32_t *reg_cycle = base + map->cycle;
  uint32_t *reg_trigh = base + map->trig_h;
  uint32_t *reg_trigl = base + map->trig_l;
  uint64_t t_total;
  uint32_t *reg_latch = &dio->LATCH;

  if (cmd->tm_ns < 16){
    cmd->time_posix=cmd->time_posix-1;
    cmd->tm_ns = 0x3b9ac9f;
  }else{
    cmd->tm_ns = (cmd->tm_ns/16)-1;
  }

  *reg_cycle = cmd->tm_ns;
  *reg_trigl = cmd->time_posix;
  *reg_trigh = ((cmd->time_posix >> 32) & 0xffffffff); //Only 32 bits for time_posix
  t_total = *reg_trigl|*reg_trigl;
  *reg_latch = 1 << ch;

  /*
  mprintf("Value 0x%8x written on address 0x%8x (LATCH bit)\n", *reg_latch, reg_latch); //debug
  mprintf("Value 0x%8x written on address 0x%8x (sec) (channel %i TRIGL)\n", *reg_trigl, reg_trigl, ch); //debug
  mprintf("Value 0x%8x written on address 0x%8x (sec) (channel %i TRIGH)\n", *reg_trigh, reg_trigh, ch); //debug
  mprintf("Value 0x%8x written on address 0x%8x (cyc) (channel %i cycle)\n", *reg_cycle, reg_cycle, ch); //debug */

  //mprintf("Written POSIX time ");
  //print_longer_than_32_bits(t_total);
  //mprintf("\n");


  //mprintf("Pulse generation configured\n");

}

int wrn_dio_cmd_pulse(struct wr_dio_cmd *cmd, int loop_sec, int loop_nsec)
{
  void  *base = dio;

  int ch = cmd->channel;

  map = regmap + ch;

  uint32_t *reg_per_flag = &dio->PER;
  uint32_t *reg_per_sec = base + map->per_sec;
  uint32_t *reg_per_cyc = base + map->per_cyc;
  uint32_t *reg_rdy = &dio->TRIG;


  // The old triggers are removed.
  dio_reset(1 << ch);

  /* The call to the function wrn_new_pulse() removes this value
   * so we need to store it */
  uint32_t tm_ns_value = cmd->tm_ns;

  uint32_t *reg_pulse =  base + map->pulse;

  *reg_pulse = cmd->pulse / 16;
  //mprintf("Value 0x%8x written on address 0x%8x (pulse length)\n", *reg_pulse, reg_pulse); //Debug

  if (cmd->flags == WR_DIO_F_NOW) {
    /* if "now" we are done */
    uint32_t *reg_immpulse =  &dio->PULSE;
    *reg_immpulse = 1 << ch; //Puts a 1 in the bit corresponding to the selected channel
	  return 0;
  }

  if (cmd->flags == WR_DIO_F_ABS)
    wrn_new_pulse(cmd, ch);

  if (cmd->flags == WR_DIO_F_LOOP) {
   //mprintf("Loop each %is %ins\n",loop_sec,loop_nsec); // debug

    if (*reg_rdy == 0x0f) {

      if ( (loop_nsec != 0) && (loop_nsec/16 < 6) ) {
	  mprintf(" Nanosecond periodic option must be larger than 96 ns\n");
	  return -1;
      } else if ( (loop_sec != 0) && (loop_nsec == 0) ){
	  loop_sec = loop_sec-1;
	  loop_nsec = 0x3b9ac9a;
      } else {
	  loop_nsec =loop_nsec/16 - 6;
      }

/*  if (loop_nsec/16 == 5) {
      loop_sec = loop_sec-1;
      loop_nsec = 0x3b9ac9f;
    }else if (loop_nsec/16 == 4) {
      loop_sec = loop_sec-1;
      loop_nsec = 0x3b9ac9e;
    }else if (loop_nsec/16 == 3) {
      loop_sec = loop_sec-1;
      loop_nsec = 0x3b9ac9d;
    }else if (loop_nsec/16 == 2) {
      loop_sec = loop_sec-1;
      loop_nsec = 0x3b9ac9c;
    }else if (loop_nsec/16 == 1) {
      loop_sec = loop_sec-1;
      loop_nsec = 0x3b9ac9b;
    }else if (loop_nsec/16 == 0) {
      loop_sec = loop_sec-1;
      loop_nsec = 0x3b9ac9a;
    } else loop_nsec =loop_nsec/16 - 6;
    */

    *reg_per_sec = loop_sec;
    *reg_per_cyc = loop_nsec;
    *reg_per_flag = *reg_per_flag | (1 << ch );

    /*
    mprintf("Value 0x%8x written on address 0x%8x (period flag)\n", *reg_per_flag, reg_per_flag); //debugging
    mprintf("Value 0x%8x written on address 0x%8x (seconds of period)\n", *reg_per_sec, reg_per_sec); //debugging
    mprintf("Value 0x%8x written on address 0x%8x (nanoseconds of period)\n", *reg_per_cyc, reg_per_cyc); //debugging */

    wrn_new_pulse(cmd, ch);
    }
  }

  /* The value is restored */
  cmd->tm_ns = tm_ns_value;
  return 0;
}

uint32_t parse_nanos(char *toparse){
  int aux_nano=0;
  int nanos = 0;
  int n = 0;
  char lengh_time[9];
  strncpy(lengh_time,toparse,9);
  n = strlen(toparse);
  if (n>9) {
    mprintf("Invalid nanoseconds value. Nanoseconds must be 0-999999999\n");
    return -1;
  }

  aux_nano = atoi(lengh_time);
  return aux_nano;
}
