#include "wr_zen_i2c.h"
#include "hw/memlayout.h"

volatile uint32_t *GPIO_ADDR;

static void wr_zen_gpio_set(int n_gpio) {
	* (volatile uint32_t *) ((uint32_t)GPIO_ADDR + FPGA_GPIO_SET_ADDR(n_gpio)) = FPGA_GPIO_SET_DATA(n_gpio);
}

static void wr_zen_gpio_clr(int n_gpio) {
	* (volatile uint32_t *) ((uint32_t)GPIO_ADDR + FPGA_GPIO_CLR_ADDR(n_gpio)) = FPGA_GPIO_CLR_DATA(n_gpio);

}

static void wr_zen_gpio_update(int n_gpio, int v) {
	if(v)
		wr_zen_gpio_set(n_gpio);
	else
		wr_zen_gpio_clr(n_gpio);
}

static uint32_t wr_zen_gpio_read(int n_gpio) {
	uint32_t val;
	
	val = * (volatile uint32_t *) ((uint32_t)GPIO_ADDR + FPGA_GPIO_STA_ADDR(n_gpio));
	
	return val;
}

static uint32_t wr_zen_gpio_get() {
	return wr_zen_gpio_read(0);
}

void wr_zen_gpio_init(){
	mprintf("ZEN GPIO ...  init \n");
	GPIO_ADDR = (volatile uint32_t *)BASE_WB_GPIO;
}


int read_i2c_switch(uint8_t i2cif,uint8_t sw_addr) {
	unsigned char d;
	uint8_t sw_read = (sw_addr<<1) | 0x01;
	
	mi2c_start(i2cif);

	if ((mi2c_put_byte(i2cif, sw_read)) < 0) {
		mi2c_stop(i2cif);
		mprintf("Error to read the I2C Switch.\n");
		return -1;
	}

	mi2c_get_byte(i2cif,&d,1);

	mi2c_stop(i2cif);

	return d;
}

int write_i2c_switch(uint8_t i2cif,uint8_t sw_addr, uint8_t value) {
	unsigned char d;
	uint8_t sw_write = (sw_addr<<1);

	mi2c_start(i2cif);

	if ((mi2c_put_byte(i2cif, sw_write)) < 0) {
		mi2c_stop(i2cif);
		mprintf("Error to address the I2C Switch.\n");
		return -1;
	}

	if((mi2c_put_byte(i2cif, value)) < 0) {
		mi2c_stop(i2cif);
		mprintf("Error to update the I2C Switch,\n");
		return -2;
	}

	mi2c_stop(i2cif);

	return 0;
}

int configure_i2c_switch(uint8_t i2cif,uint8_t sw_addr, uint8_t sw_config) {
	int d;

	d = read_i2c_switch(i2cif,sw_addr);

	if (d < 0)
		return -1;

	d = write_i2c_switch(i2cif,sw_addr,sw_config);

	if (d < 0)
		return -2;

	d = read_i2c_switch(i2cif,sw_addr);

	if (d < 0)
		return -3;

	return 0;
}

void wr_zen_i2c_sfp_init() {
	int i;
	
	// Configuring as outputs
	configure_i2c_pca9554(WRPC_FMC_I2C,PCA9554_ZEN_OUTPUT_I2C,0x0);
	
	write_i2c_pca9554(WRPC_FMC_I2C,PCA9554_ZEN_OUTPUT_I2C,0x00);
	
	// Configuring as inputs
	configure_i2c_pca9554(WRPC_FMC_I2C,PCA9554_ZEN_INPUT_I2C,0x0F);
	
	// Init the first four GPIOs to '1' (active in low)... but the GTP_RTS and SW_GTP
	for(i = 0 ; i < N_GPIOS-2 ; i++)
		wr_zen_gpio_set(i);
}

static void wr_zen_i2c_gpio_set(int gpio_addr, int n_gpio) {
	unsigned char d;
	
	d = read_i2c_pca9554(WRPC_FMC_I2C,gpio_addr);
	write_i2c_pca9554(WRPC_FMC_I2C,gpio_addr,d | GEN_OR_MASK(n_gpio));
}

static void wr_zen_i2c_gpio_clr(int gpio_addr, int n_gpio) {
	unsigned char d;
	
	d = read_i2c_pca9554(WRPC_FMC_I2C,gpio_addr);
	write_i2c_pca9554(WRPC_FMC_I2C,gpio_addr,(d & GEN_AND_MASK(n_gpio)));
}

static void wr_zen_i2c_gpio_update(int gpio_addr, int n_gpio, int v) {
	if(v) 
		wr_zen_i2c_gpio_set(gpio_addr, n_gpio);
	else
		wr_zen_i2c_gpio_clr(gpio_addr, n_gpio);
}

void wr_zen_link_led(int state, int nsfp) {
	if(state) {
		wr_zen_i2c_gpio_set(PCA9554_ZEN_OUTPUT_I2C, (nsfp == 0 ? WR_LINK1_LED_LINK : WR_LINK2_LED_LINK));
	} else {
		wr_zen_i2c_gpio_clr(PCA9554_ZEN_OUTPUT_I2C, (nsfp == 0 ? WR_LINK1_LED_LINK : WR_LINK2_LED_LINK));
	}
}

void wr_zen_sfp_i2c_update(uint8_t initial_check) {
	/* sfp_in: Signals from SFPs (SFP_DETECT and SFP_TX_LOS) */
	unsigned char sfp_in;
	/* sfp_out: Signals to SFPs (LEDs and SFP_TX_DIS) */
	unsigned char sfp_out;
	unsigned char i2c_sw;
	static uint32_t t = 0;
	uint32_t gpio_out;
	
	int sfp0_det, sfp1_det, sfp0_tx_los, sfp1_tx_los;
	int sfp0_tx_dis, sfp1_tx_dis;
	
	/* For returning, the timeout should expire and the function call shouldn't be the initial */
	if ((timer_get_tics() - t < I2C_REFRESH_TIME) && (initial_check == 0))
		return;
	
	t = timer_get_tics();
	
	/* warn: This is only for testing purposes */
	i2c_sw = read_i2c_switch(WRPC_FMC_I2C,I2C_SWITCH_ADDR);
	
	/* Read the SFP inputs (SFP_DETECT and SFP_TX_LOS) from the PCA9554 */
	sfp_in = read_i2c_pca9554(WRPC_FMC_I2C,PCA9554_ZEN_INPUT_I2C);
	
	/* Extract each one in a variable */
	sfp0_det = GET_GPIO(sfp_in, WR_LINK1_DETECT);
	sfp1_det = GET_GPIO(sfp_in, WR_LINK2_DETECT);
	sfp0_tx_los = GET_GPIO(sfp_in, WR_LINK1_LOS_TX_FAULT);
	sfp1_tx_los = GET_GPIO(sfp_in, WR_LINK2_LOS_TX_FAULT);
	
	//mprintf("I2C_SW: 0x%x, GPIOs: 0x%x => sfp0_det: 0x%x, sfp1_det: 0x%x, sfp0_tx_los: 0x%x, sfp1_tx_los: 0x%x \n",i2c_sw,sfp_in,sfp0_det,sfp1_det,sfp0_tx_los,sfp1_tx_los);
	
	/* Set the WB GPIO to pass to the hardware */
	wr_zen_gpio_update(WB_GPIO_SFP1_DET, sfp0_det);
	wr_zen_gpio_update(WB_GPIO_SFP1_TX_LOS_FAULT, sfp0_tx_los);
	wr_zen_gpio_update(WB_GPIO_SFP2_DET, sfp1_det);
	wr_zen_gpio_update(WB_GPIO_SFP2_TX_LOS_FAULT, sfp1_tx_los);
		
	/* Now, read the WB GPIO to get SFP_TX_DIS signal for SFP0 and SFP1 */
	gpio_out = wr_zen_gpio_get();
	sfp0_tx_dis = GET_GPIO(gpio_out, WB_GPIO_SFP1_TX_DIS);
	sfp1_tx_dis = GET_GPIO(gpio_out, WB_GPIO_SFP2_TX_DIS);
	
	/* Finally, update the PC9554 to pass values to SFP modules */
	wr_zen_i2c_gpio_update(PCA9554_ZEN_OUTPUT_I2C, SFP1_TX_DISABLE, sfp0_tx_dis);
	wr_zen_i2c_gpio_update(PCA9554_ZEN_OUTPUT_I2C, SFP2_TX_DISABLE, sfp1_tx_dis);

}

void wr_zen_gpio_gtp_rst(){
	wr_zen_gpio_set(WB_GPIO_GTP_RESET);
	wr_zen_gpio_clr(WB_GPIO_GTP_RESET);
}
