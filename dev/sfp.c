/*
 * This work is part of the White Rabbit project
 *
 * Copyright (C) 2012 CERN (www.cern.ch)
 * Author: Tomasz Wlostowski <tomasz.wlostowski@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 */
/* SFP Detection / managenent functions */

#include <stdio.h>
#include <inttypes.h>

#include "syscon.h"
#include "i2c.h"
#include "sfp.h"
#include "eeprom.h"
#include "wrc.h"

#ifdef CONFIG_ZEN_BOARD
#include "wr_zen_i2c.h"
#endif

int8_t sfp_detected[2] = { 0 , 0 };

int sfp_present(int port)
{
	/* sfpX_det is grounded by the SFP to indicate that the module is present */

	if(port == 0)
		return !gpio_in(GPIO_SFP0_DET);
	else if(port==1)
		return !gpio_in(GPIO_SFP1_DET);
}

int sfp_read_part_id(char *part_id, int port)
{
	int i;
	uint8_t data, sum;
	unsigned char sw_conf, sw_conf_dfl;

	if(port==0) {

		#ifdef CONFIG_ZEN_BOARD
			sw_conf_dfl = (unsigned char) read_i2c_switch(WRPC_SFP0_I2C,I2C_SWITCH_ADDR);
			//sw_conf = sw_conf_dfl | 0x1;
			sw_conf = 0x2;
			configure_i2c_switch(WRPC_SFP0_I2C,I2C_SWITCH_ADDR,sw_conf);
		#endif

		mi2c_init(WRPC_SFP0_I2C);

		mi2c_start(WRPC_SFP0_I2C);
		mi2c_put_byte(WRPC_SFP0_I2C, 0xA0);
		mi2c_put_byte(WRPC_SFP0_I2C, 0x00);
		mi2c_repeat_start(WRPC_SFP0_I2C);
		mi2c_put_byte(WRPC_SFP0_I2C, 0xA1);
		mi2c_get_byte(WRPC_SFP0_I2C, &data, 1);
		mi2c_stop(WRPC_SFP0_I2C);

		sum = data;

		mi2c_start(WRPC_SFP0_I2C);
		mi2c_put_byte(WRPC_SFP0_I2C, 0xA1);
		for (i = 1; i < 63; ++i) {
			mi2c_get_byte(WRPC_SFP0_I2C, &data, 0);
			sum = (uint8_t) ((uint16_t) sum + data) & 0xff;
			if (i >= 40 && i <= 55) {	//Part Number 
				part_id[i - 40] = data;
			}

		}
		mi2c_get_byte(WRPC_SFP0_I2C, &data, 1);	//final word, checksum
		mi2c_stop(WRPC_SFP0_I2C);
		
		#ifdef CONFIG_ZEN_BOARD
			//sw_conf &= ~0x1;
			sw_conf = sw_conf_dfl;
			configure_i2c_switch(WRPC_SFP0_I2C,I2C_SWITCH_ADDR,sw_conf);
		#endif

	} else if (port == 1){
		
		#ifdef CONFIG_ZEN_BOARD
			sw_conf_dfl = (unsigned char) read_i2c_switch(WRPC_SFP1_I2C,I2C_SWITCH_ADDR);
			//sw_conf = sw_conf_dfl | 0x2;
			sw_conf = 0x1;
			configure_i2c_switch(WRPC_SFP1_I2C,I2C_SWITCH_ADDR,sw_conf);
		#endif

		mi2c_init(WRPC_SFP1_I2C);

		mi2c_start(WRPC_SFP1_I2C);
		mi2c_put_byte(WRPC_SFP1_I2C, 0xA0);
		mi2c_put_byte(WRPC_SFP1_I2C, 0x00);
		mi2c_repeat_start(WRPC_SFP1_I2C);
		mi2c_put_byte(WRPC_SFP1_I2C, 0xA1);
		mi2c_get_byte(WRPC_SFP1_I2C, &data, 1);
		mi2c_stop(WRPC_SFP1_I2C);

		sum = data;

		mi2c_start(WRPC_SFP1_I2C);
		mi2c_put_byte(WRPC_SFP1_I2C, 0xA1);
		for (i = 1; i < 63; ++i) {
			mi2c_get_byte(WRPC_SFP1_I2C, &data, 0);
			sum = (uint8_t) ((uint16_t) sum + data) & 0xff;
			if (i >= 40 && i <= 55) //Part Number
				part_id[i - 40] = data;
		}
		mi2c_get_byte(WRPC_SFP1_I2C, &data, 1);	//final word, checksum
		mi2c_stop(WRPC_SFP1_I2C);
		
		#ifdef CONFIG_ZEN_BOARD
			//sw_conf &= ~0x2;
			sw_conf = sw_conf_dfl;
			configure_i2c_switch(WRPC_SFP1_I2C,I2C_SWITCH_ADDR,sw_conf);
		#endif
	}

	if (sum == data)
		return 0;

	return -1;
}

int sfp_detect_and_match(uint8_t port){

	struct s_sfpinfo sfp;
	static char pn[wrlen_num_ports][SFP_PN_LEN + 1] = {"\0"};

	/* Detecting */
	if (sfp_present(port) && !sfp_detected[port]) {
		timer_delay(200);

		sfp_read_part_id(pn[port], port);
		mprintf("SFP %u detected: %s\n",(port + 1), pn[port]);
		sfp_detected[port] = 1;

		/* Matching */
		strncpy(sfp.pn, pn[port], SFP_PN_LEN);
		if (eeprom_match_sfp(WRPC_FMC_I2C, FMC_EEPROM_ADR, &sfp, port) > 0) {
			mprintf("SFP %u matched dTx=%d, dRx=%d, alpha=%d\n", (port + 1),  sfp.dTx, sfp.dRx, sfp.alpha);
			sfp_deltaTx[port] = sfp.dTx;
			sfp_deltaRx[port] = sfp.dRx;
			sfp_alpha[port] = sfp.alpha;

		} else {
			mprintf("Could not match SFP %u to DB\n", (port + 1));
			sfp_deltaTx[port] = SFP_DELTATX_DEFAULT;
			sfp_deltaRx[port] = SFP_DELTARX_DEFAULT;
			sfp_alpha[port] = SFP_ALPHA_DEFAULT;
			mprintf("SFP %u matched to the by default params. dTx=%d, dRx=%d, alpha=%d\n", (port + 1),  sfp_deltaTx[port], sfp_deltaRx[port], sfp_alpha[port]);
		}

	/* Removing */
	} else if (!sfp_present(port) && sfp_detected[port] ){
		mprintf("SFP %u removed \n", (port + 1));
		sfp_detected[port] = 0;
	}

	return 0;
}
