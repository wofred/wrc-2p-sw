/*
 *  This work is part of the WR-LEN project. 2016
 *
 * Author: Emilio Marín López <emilio<at>sevensols.com>
 *
 * Library to handle the writing into the PCA9554 which handles the Fine Tune adjustments for the output triggers.
 *
 */

#include <stdio.h>
#include "len_fine_tune.h"
#include "hw/memlayout.h"
#include "hw/lentrig_regs.h"
#include "syscon.h"
#include "eeprom.h"

//#include "wr-dio.h" //wr_dio_cmd struct.
//volatile struct wr_dio_cmd *cmd_pps_config;

volatile struct wr_dio_cmd _cmd_pps_config;
volatile struct wr_dio_cmd *cmd_pps_config = &_cmd_pps_config;

struct s_wr_len_trigger len_trigger;

volatile uint32_t *GPIO_ADDR;
volatile struct LENTRIG_WB *LENTRIG;

void wr_len_gpio_set(int n_gpio) {
	* (volatile uint32_t *) ((uint32_t)GPIO_ADDR + FPGA_GPIO_SET_ADDR(n_gpio)) = FPGA_GPIO_SET_DATA(n_gpio);
}

void wr_len_gpio_clr(int n_gpio) {
	*(volatile uint32_t *) ((uint32_t)GPIO_ADDR + FPGA_GPIO_CLR_ADDR(n_gpio)) = FPGA_GPIO_CLR_DATA(n_gpio);

}

/* This function initializes the DB9 PPS output */
void wr_len_init_pps_db9(){
  uint8_t pps_ch_out = 0;
  uint64_t sec = 0;
  uint32_t nsec = 0;
  uint32_t pulse_length = 20000000; // nanoseconds of length

  len_trigger.pps_nsec_offset = 0;
  len_trigger.pps_i2c_delay = 0;
  len_trigger.clk_shift = 0;
  len_trigger.clk_i2c_delay = 0;

  //Update the values from the EEPROM in case that it's written.
   if (eeprom_read_parameters(WRPC_FMC_I2C, FMC_EEPROM_ADR, &len_trigger) == -1)
     mprintf("WR-LEN DB9: EEPROM is empty \n");

  //cmd_pps_config = &cmd_pps;

  cmd_pps_config->time_posix = sec;
  cmd_pps_config->tm_ns = nsec;
  cmd_pps_config->flags = WR_DIO_F_LOOP;	// One pulse per second.
  cmd_pps_config->channel = pps_ch_out;
  cmd_pps_config->pulse = pulse_length;
}

/* Initialize the WR-LEN Connector triggers */
void wr_len_connector_init(){

  /* The DIO module is initialized */
  dio_init();

  /* Initialize the I2C bus */
  mprintf("LEN GPIO ...  init \n");
  GPIO_ADDR = (volatile uint32_t *) BASE_WB_GPIO;

  /* Initialize I2C bus */
  mi2c_init(WR_LEN_CONN_I2C);

  /* Configuring as outputs. 0 means output */
  configure_i2c_pca9554(WR_LEN_CONN_I2C, PCA9554_LEN_FT_I2C, 0x00);

  /* Initialize the WR-LEN triggers module */
  mprintf("LEN Trigger output handling ...  init \n");
  LENTRIG = (volatile struct LENTRIG_WB *) BASE_WB_LENTRIG;

  /* Initialize the WR-LEN trigger configuration register. The Fine adjustement bits are locked by default. */
  LENTRIG->CONFIG = LENTRIG_CONFIG_CONN_FT1_ADJ | LENTRIG_CONFIG_CONN_FT2_ADJ;

  /* Default value */
  wr_len_i2c_write(DATA_CLK_OUTPUT_CH, PCA9554_I2C_DEFAULT_VALUE);
  wr_len_i2c_write(PPS_OUTPUT_CH, PCA9554_I2C_DEFAULT_VALUE);

  /* Initialize the DB9 PPS Output*/
  wr_len_init_pps_db9();
}

/* This function populates the wr_len_trigger struct and stored the parameters at the external eeprom*/
void wr_len_save_parameters(uint32_t pps_coarse, uint8_t pps_fine, uint8_t clk_coarse, uint8_t clk_fine, uint8_t save){
  len_trigger.pps_nsec_offset = pps_coarse;
  len_trigger.pps_i2c_delay = pps_fine;
  len_trigger.clk_shift = clk_coarse;
  len_trigger.clk_i2c_delay = clk_fine;

  mprintf(" Config values: \n  PPS coarse = %i; PPS Fine Tune = 0x%02x\n  CLK coarse = %i; CLK Fine Tune = 0x%02x \n",
	    len_trigger.pps_nsec_offset, len_trigger.pps_i2c_delay, len_trigger.clk_shift, len_trigger.clk_i2c_delay);

  /* Store the params at the eeprom? */
  if (save == 1)
      eeprom_store_parameters(WRPC_FMC_I2C, FMC_EEPROM_ADR, &len_trigger);
}

/* This function configures the DB9 PPS Output */
void wr_len_configure_pps_db9(uint32_t nsec_offset){
  uint32_t l_sec = 1;		// One pulse per second.
  uint32_t l_nsec = 0;

  uint64_t sec = 0;
  uint32_t nsec = 0;
  shw_pps_gen_get_time(&sec, &nsec); 	// Get the WR time

  cmd_pps_config->time_posix = sec + PPS_TIME_GAP;	// The PPS will be raised in 1 second from now on.
  cmd_pps_config->tm_ns = nsec_offset;

  /* Reseting the old triggers */
  dio_reset(0x1f);

  /* The output is configured through this pulse */
  if (wrn_dio_cmd_pulse(cmd_pps_config,l_sec,l_nsec) < 0)
    mprintf(" Error pulsing\n");
}

/* This function syncs the DB9 PPS output without losing its original config */
int wr_len_sync_pps_db9(){
  uint32_t l_sec = 1;		// One pulse per second.
  uint32_t l_nsec = 0;

  uint64_t sec = 0;
  uint32_t nsec = 0;

  /* Reseting the old triggers */
  dio_reset(0x1f);

  // PPS_CLK is enabled.
  if (LENTRIG->CONFIG & LENTRIG_CONFIG_CONN_PPS_CLK_EN == LENTRIG_CONFIG_CONN_PPS_CLK_EN){
    shw_pps_gen_get_time(&sec, &nsec); 		// Get the WR time
    cmd_pps_config->time_posix = sec + PPS_TIME_GAP;	// The PPS will be raised in 1 second from now on.

    /* The output is configured through this pulse */
    if (wrn_dio_cmd_pulse(cmd_pps_config,l_sec,l_nsec) < 0)
      mprintf(" Error pulsing\n");

    return 1;
  } else // PPS_CLK is disabled so nothing to do.
    return 0;

}

/* The Fine Tune ICs update the delay value from their I2C lines. One falling edge for updating it */
void wr_len_fine_update(uint8_t channel){
  // Original
  LENTRIG->CONFIG = LENTRIG->CONFIG | ( 1 << 1 + channel);  //write 1 into the Fine Tune <channel> adjustment bit.
  LENTRIG->CONFIG = LENTRIG->CONFIG & ~( 1 << 1 + channel); //write 0 into the Fine Tune <channel> adjustment bit.
  LENTRIG->CONFIG = LENTRIG->CONFIG | ( 1 << 1 + channel);  //write 1 into the Fine Tune <channel> adjustment bit.

}

/* Triggering the soft SYNC at the CLK output counter */
void wr_len_sync_clk() {

  LENTRIG->CONFIG = LENTRIG->CONFIG | LENTRIG_CONFIG_CONN_PPS_CLK_SYNC;
  usleep(10);
  LENTRIG->CONFIG = LENTRIG->CONFIG & ~(LENTRIG_CONFIG_CONN_PPS_CLK_SYNC);

}

/* For writing into the Counter SYNC register, the number of shifts after SYNC */
void wr_len_shift_clk_wr(uint8_t value){

  LENTRIG->CONFIG = (LENTRIG->CONFIG & ~LENTRIG_CONFIG_CTR_SYNC_VALUE_MASK) | LENTRIG_CONFIG_CTR_SYNC_VALUE_W(value);

}

/* Enabling/disabling the DB9 PPS & CLK outputs */
void wr_len_pps_and_clk_ena(uint8_t ena){

  if (ena == 1) {

    //Enabling the CONN_PPS_CLK_EN bit.
    LENTRIG->CONFIG = LENTRIG->CONFIG | LENTRIG_CONFIG_CONN_PPS_CLK_EN;

    //... and configuring the PPS & CLK
    wr_len_configure_pps_db9(len_trigger.pps_nsec_offset);
    wr_len_i2c_write(PPS_OUTPUT_CH, len_trigger.pps_i2c_delay);
    wr_len_shift_clk_wr(len_trigger.clk_shift);
    wr_len_i2c_write(DATA_CLK_OUTPUT_CH, len_trigger.clk_i2c_delay);
    wr_len_sync_pps_db9();	// Sync PPS
    wr_len_sync_clk();		// Sync CLK

    mprintf(" Config values: \n  PPS coarse = %i; PPS Fine Tune = 0x%02x\n  CLK coarse = %i; CLK Fine Tune = 0x%02x \n",
	    len_trigger.pps_nsec_offset, len_trigger.pps_i2c_delay, len_trigger.clk_shift, len_trigger.clk_i2c_delay);

  } else {
    //Disabling the CONN_PPS_CLK_EN bit.
    wr_len_i2c_write(DATA_CLK_OUTPUT_CH, PCA9554_I2C_DEFAULT_VALUE);    //data_clk
    wr_len_i2c_write(PPS_OUTPUT_CH, PCA9554_I2C_DEFAULT_VALUE);         //pps
    LENTRIG->CONFIG = LENTRIG->CONFIG & ~(LENTRIG_CONFIG_CONN_PPS_CLK_EN);
    dio_reset(0x1f); // Disable the PPS DB9 output
  }
}

void wr_len_i2c_write(uint8_t channel, uint8_t value){

  /* Configuring as outputs. 0 means output */
  configure_i2c_pca9554(WR_LEN_CONN_I2C, PCA9554_LEN_FT_I2C, 0x00);

  /* Default value */
  write_i2c_pca9554(WR_LEN_CONN_I2C, PCA9554_LEN_FT_I2C, value);

  /* Update the Fine-delay IC */
  wr_len_fine_update(channel);
}
