
#include "wr_zen_cta_gpio.h"
#include "hw/memlayout.h"

volatile uint32_t *GPIO_ADDR;

static void wr_zen_gpio_set(int n_gpio) {
	* (volatile uint32_t *) ((uint32_t)GPIO_ADDR + FPGA_GPIO_SET_ADDR(n_gpio)) = FPGA_GPIO_SET_DATA(n_gpio);
}

static void wr_zen_gpio_clr(int n_gpio) {
	* (volatile uint32_t *) ((uint32_t)GPIO_ADDR + FPGA_GPIO_CLR_ADDR(n_gpio)) = FPGA_GPIO_CLR_DATA(n_gpio);

}

void wr_zen_gpio_init(){

	mprintf("ZEN CTA GPIO ...  init \n");
	GPIO_ADDR = (volatile uint32_t *)BASE_WB_GPIO;
}

void wr_zen_gpio_gtp_rst(){
	wr_zen_gpio_set(WB_GPIO_GTP_RESET);
	usleep(1);
	wr_zen_gpio_clr(WB_GPIO_GTP_RESET);
}
